package com.creditcards.cccomus.runner;

import org.jbehave.core.io.StoryFinder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.List;

import static org.jbehave.core.io.CodeLocations.codeLocationFromPath;

public class StoryLoader{
    private final static Logger logger = LoggerFactory.getLogger(StoryLoader.class);
    private static final String STORY_DIRECTORY = "stories/";

    public List<String> storyPaths(List<String> includes, List<String> excludes){
        String currentDirectory = new File("").toURI().getPath();
        String storyDirectory = currentDirectory.endsWith("classes/") ? "stories/" : "target/classes/stories/";

        String searchInDirectory = codeLocationFromPath(storyDirectory).getFile();
        logger.info("   ** Story Search Directory:" + searchInDirectory);
        return new StoryFinder().findPaths(searchInDirectory,includes,excludes,StoryLoader.STORY_DIRECTORY);
    }
}

