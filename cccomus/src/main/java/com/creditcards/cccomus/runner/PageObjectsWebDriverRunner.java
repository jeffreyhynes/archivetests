package com.creditcards.cccomus.runner;

import com.creditcards.cccomus.webdriver.pages.CreditScoreEstimatorPage;
import com.creditcards.cccomus.webdriver.pages.HomePage;
import org.jbehave.web.selenium.PropertyWebDriverProvider;
import org.jbehave.web.selenium.WebDriverProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class PageObjectsWebDriverRunner{
    private final static Logger logger = LoggerFactory.getLogger(PageObjectsWebDriverRunner.class);
    private WebDriverProvider driverProvider;

    @BeforeMethod
    public void setUp() throws Exception{
        System.setProperty("browser","firefox");
        driverProvider = new PropertyWebDriverProvider();
        driverProvider.initialize();
    }

    @Test
    public void executeWebDriverPageObjects(){
        logger.info("Create the Home Page Object and navigate to the website");
        HomePage homePage = new HomePage(driverProvider);
        homePage.go();

        logger.info("Wait for the pages to load");
        homePage.waitUntilLoaded();

        logger.info("HomePage is displayed");
        assertTrue(homePage.isDisplayed());

        logger.info("Icon is displayed");
        assertTrue(homePage.isIconDisplayed());

        logger.info("Icon is displayed Using css Selector");
        assertTrue(homePage.isLogoCssDisplayed());

        logger.info("Not Sure link is displayed");
        assertTrue(homePage.isNotSureLinkDisplayed());

        logger.info("Click the Not Sure Search Link");
        homePage.clickNotSureLink();

        logger.info("Wait for the Credit Score Estimator Page to open");
        CreditScoreEstimatorPage creditScoreEstimatorPage = new CreditScoreEstimatorPage(driverProvider);
        creditScoreEstimatorPage.waitUntilLoaded();

        logger.info("Assert BreadCrumb Trail");
        assertTrue(creditScoreEstimatorPage.isBreadCrumbDisplayed("Credit Score Estimator"));

        String title = "Estimate your Credit Score for FREE";
        String title2 = "Estimate your Credit\nScore for FREE";
        String content = "Answer the questions below to view your estimated credit score range. Instantly see your results as well as tips on how to improve your credit score. Find a list of credit cards that match your estimated credit score.";

        logger.info("Assert Page Header");
        assertTrue(creditScoreEstimatorPage.headerEquals(title2,content));

        logger.info("Select Question 1");
        creditScoreEstimatorPage.selectQuestion1(CreditScoreEstimatorPage.Question1.TWO_TO_FOUR);

        logger.info("Select Question 1a");
        creditScoreEstimatorPage.selectQuestion1a("between 6 months and 2 years ago");
    }
}
