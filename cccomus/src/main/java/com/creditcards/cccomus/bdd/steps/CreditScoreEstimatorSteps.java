package com.creditcards.cccomus.bdd.steps;

import com.creditcards.cccomus.webdriver.pages.CreditScoreEstimatorPage;
import com.creditcards.steps.Steps;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.web.selenium.WebDriverProvider;
import org.springframework.beans.factory.annotation.Autowired;

import static org.testng.Assert.assertTrue;

@Steps
public class CreditScoreEstimatorSteps{
    private final WebDriverProvider webDriverProvider;
    private CreditScoreEstimatorPage creditScoreEstimatorPage;

    @Autowired
    public CreditScoreEstimatorSteps(WebDriverProvider webDriverProvider){
        this.webDriverProvider = webDriverProvider;
    }

    @Then("the Credit Score Estimator pages is displayed")
    private void pageIsLoaded(){
        creditScoreEstimatorPage = new CreditScoreEstimatorPage(webDriverProvider);
        creditScoreEstimatorPage.waitUntilLoaded();
        assertTrue(creditScoreEstimatorPage.isDisplayed());
    }

    @When("I select the answer 2 to 4 for question 1")
    private void selectQuestion1(){
        creditScoreEstimatorPage.selectQuestion1(CreditScoreEstimatorPage.Question1.TWO_TO_FOUR);
    }

    @Then("question 1.a is displayed")
    private void question1aIsDisplayed(){
        assertTrue(creditScoreEstimatorPage.isQuestion1aDisplayed());
    }

    @When("I select the answer between 6 months and 2 years ago for question 1a")
    private void selectQuestion1a(){
        creditScoreEstimatorPage.selectQuestion1a("between 6 months and 2 years ago");
    }

    @When("I select the answer $option for question 2")
    private void selectQuestion2(String option){
        creditScoreEstimatorPage.selectQuestion2(option);
    }

    @When("I select the answer $option for question 3")
    private void selectQuestion3(String option){
        creditScoreEstimatorPage.selectQuestion2(option);
    }

    @When("I select the answer $option for question 4")
    private void selectQuestion4(String option){
        creditScoreEstimatorPage.selectQuestion2(option);
    }

    @When("I select the answer $option for question 5")
    private void selectQuestion5(String option){
        creditScoreEstimatorPage.selectQuestion2(option);
    }

    @When("I select the answer $option for question 6")
    private void selectQuestion6(String option){
        creditScoreEstimatorPage.selectQuestion2(option);
    }

    @When("I select the answer $option for question 7")
    private void selectQuestion7(String option){
        creditScoreEstimatorPage.selectQuestion2(option);
    }

    @When("I select the answer $option for question 8")
    private void selectQuestion8(String option){
        creditScoreEstimatorPage.selectQuestion2(option);
    }

    @When("I select the answer $option for question 8a")
    private void selectQuestion8a(String option){
        creditScoreEstimatorPage.selectQuestion2(option);
    }

    @When("I select the answer $option for question 9")
    private void selectQuestion9(String option){
        creditScoreEstimatorPage.selectQuestion2(option);
    }

    @When("I select the answer $option for question 10")
    private void selectQuestion10(String option){
        creditScoreEstimatorPage.selectQuestion2(option);
    }

    @When("I click the View Results Button")
    private void clickViewResults(){
        creditScoreEstimatorPage.clickViewResults();
    }
}
