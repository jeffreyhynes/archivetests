BDD aka
User Acceptance Test

Key Points
Software delivery is about writing software to achieve business outcomes.

Behaviour Driven Design (BDD)
•	Simply it is a language to ensure everyone knows what’s going on.
•	Foundation is conversation - Communication.
•	Prevent Tower of babel

Goal
Is to develop a shared understanding of the features to be implemented.
•	In order to write tests you need understand what are you going to test.
•	Think more about business requirement before they start to write any code.

Getting the words right
•	Key focus is “Getting the words right”
The words you use influence the way you think about something.


Summary
•	A story’s behaviour is simply its acceptance criteria
•	If the system fulfills all the acceptance criteria, it’s behaving correctly; if it doesn’t, it isn’t.
•	The business value those features provide along with a set of acceptance criteria for each feature so that we know when we are "done".

Where to start
First
Define teh Buisness requirement
   As a Role
   I request a Feature
   To gain a Benefit


The speaker, who holds the Role,
is the person who will gain the Benefit
from the requested Feature

JBehave is a framework for Behaviour-Driven Development
It shifts the vocabulary from being test-based to behaviour-based, and positions itself as a design philosophy.

User Stories
Features identified are represented as User Stories

Once a story has been identified we then focus on the scenarios that
describe how the user expects the system to behave using a sequence of steps in the following form:
Given [Context]
When [Event Occurs]
Then  [Outcome]
The scenarios defined for a given story provide the acceptance criteria which will be used to determine when the feature is complete.

This, then, is the role of a Story.

It has to be a description of a requirement
and its business benefit,
and a set of criteria by which we all agree that it is “done”.


The narrative should include a role, a feature and a benefit
The template “As a [role] I want [feature] so that [benefit]“ has a number of advantages.

By specifying the role within the narrative, you know who to talk to about the feature.

By specifying the benefit, you cause the story writer to consider why they want a feature.

It gets interesting if you find the feature won’t actually deliver the benefit attributed to it.

Scenario
•	Story is defined,
•	Understand why the feature is needed, and
•	Now describe how the user expects the system to behave.
•	Use a sequence of steps.
•	Provide the acceptance criteria that will be used to determine when the feature is complete.


Title (one line describing the story)

Narrative:
As a [role]
I want [feature]
So that [benefit]

Acceptance Criteria: (presented as Scenarios)

Scenario 1: Title
Given [context]
  And [some more context]...
When  [event]
Then  [outcome]
  And [another outcome]...

Scenario 2: ...


