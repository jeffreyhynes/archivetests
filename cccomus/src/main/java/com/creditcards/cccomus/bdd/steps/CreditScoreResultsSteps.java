package com.creditcards.cccomus.bdd.steps;

import com.creditcards.cccomus.Products;
import com.creditcards.steps.Steps;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;
import org.jbehave.web.selenium.WebDriverProvider;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Vector;

@Steps
public class CreditScoreResultsSteps{
    private final WebDriverProvider webDriverProvider;

    @Autowired
    public CreditScoreResultsSteps(WebDriverProvider webDriverProvider){
        this.webDriverProvider = webDriverProvider;
    }

    @When("I click the link Advertising Disclosure")
    public void clickAdvertisingDisclosureLink(){

    }

    @Then("the disclosure dialog box is displayed")
    public void verifyDialogBoxIsDisplayed(){

    }

    @When("I click Close link in the disclosure dialog box")
    public void clickClose(){

    }

    @Then("the disclosure dialog box is hidden")
    public void verifyDialogBoxClosed(){

    }

    @Then("the Good Estimated Credit Score pages is displayed")
    public void pageIsDisplayed(){

    }

    @Then("the Estimated Credit Score is Good")
    public void verifyCreditScoreType(){

    }

    @Then("the Estimated Credit Score value is from 655-695")
    public void verifyCreditScoreValue(){

    }

    @Then("the top ten matching cards displayed are:$productList")
    public void verifyProductList(ExamplesTable productList){
        List<Products> expectList = parseExamplesTable(productList);
    }

    private List<Products> parseExamplesTable(ExamplesTable transactionTable){
        List<Products> productsList = new Vector<>();
        List<Map<String, String>> rows = transactionTable.getRows();
        int index = 0;
        for(ListIterator<Map<String, String>> listIterator = rows.listIterator();listIterator.hasNext();){
            productsList.add(index,newProduct(listIterator.next()));
            index = listIterator.nextIndex();
        }
        return productsList;
    }

    private Products newProduct(Map<String, String> productDetails){
        String name = productDetails.get("name");
        String image = productDetails.get("image");
        return new Products(name,image);
    }
}