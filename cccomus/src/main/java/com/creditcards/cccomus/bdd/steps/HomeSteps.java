package com.creditcards.cccomus.bdd.steps;

import com.creditcards.cccomus.webdriver.pages.HomePage;
import com.creditcards.steps.Steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.When;
import org.jbehave.web.selenium.WebDriverProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import static org.testng.Assert.assertTrue;

@Steps
public class HomeSteps{
    private final static Logger logger = LoggerFactory.getLogger(HomeSteps.class);
    private final WebDriverProvider webDriverProvider;

    private HomePage homePage;

    @Autowired
    public HomeSteps(WebDriverProvider webDriverProvider){
        this.webDriverProvider = webDriverProvider;
    }


    @Given("I open the website creditcards.com")
    private void openWebsite(){
        logger.info("Create the Home Page Object and navigate to the website");
        homePage = new HomePage(this.webDriverProvider);
        homePage.waitUntilLoaded();
        logger.info("HomePage is displayed");
        assertTrue(homePage.isDisplayed());

        logger.info("Icon is displayed");
        assertTrue(homePage.isIconDisplayed());

        logger.info("Icon is displayed Using css Selector");
        assertTrue(homePage.isLogoCssDisplayed());

        logger.info("Not Sure link is displayed");
        assertTrue(homePage.isNotSureLinkDisplayed());
    }

    @When("I click the Search by Credit Quality link Not Sure")
    private void selectSearchCredQualityLink(){
        logger.info("Click the Not Sure Search Link");
        homePage.clickNotSureLink();
    }
}
