package com.creditcards.cccomus;

public class Products{
    private String name;
    private String image;

    public Products(String name,String image){
        this.image = image;
        this.name = name;
    }

    public String getImage(){
        return image;
    }

    public void setImage(String image){
        this.image = image;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }
}
