package com.creditcards.cccomus.webdriver.pages;

import com.google.common.base.Function;
import org.jbehave.web.selenium.WebDriverPage;
import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage extends WebDriverPage{

    @FindBys({@FindBy(id = "skeleton"),@FindBy(className = "topbar")})
    private WebElement topbar;

    @FindBys({@FindBy(id = "skeleton"),@FindBy(className = "topbar"),@FindBy(css = "img[alt='CreditCards.com']")})
    private WebElement logo;

    @FindBys({@FindBy(css = "table.topbar img[alt='CreditCards.com']")})
    private WebElement logoCss;

    @FindBy(id = "leftnav")
    WebElement leftnav;

    @FindBys({@FindBy(id = "leftnav"),@FindBy(linkText = "Not Sure?")})
    private WebElement notSureLink;

    public HomePage(WebDriverProvider driverProvider){
        super(driverProvider);
    }

    public boolean isIconDisplayed(){
        return elementIsDisplayed(logo);
    }

    public boolean isLogoCssDisplayed(){
        return elementIsDisplayed(logoCss);
    }

    public boolean isNotSureLinkDisplayed(){
        return elementIsDisplayed(notSureLink);
    }

    public void clickNotSureLink(){
        notSureLink.click();
    }

    public void go(){
        getDriverProvider().get().get("http://www.creditcards.com");
    }

    public void initElements(){
        int DRIVER_WAIT = 5;
        PageFactory.initElements(new AjaxElementLocatorFactory(getDriverProvider().get(),DRIVER_WAIT),this);
    }

    public void waitUntilLoaded(){
        initElements();
        new WebDriverWait(getDriverProvider().get(),20)
                .until(refreshed(ExpectedConditions.visibilityOf(displayElement())));
    }

    public By displayLocator(){
        return By.cssSelector("table.topbar");
    }

    public WebElement displayElement(){
        return topbar;
    }

    public boolean isDisplayed(){
        return null != topbar && topbar.isDisplayed();
    }

    public boolean elementIsDisplayed(WebElement element){
        boolean result;
        try{
            result = null != element && element.isDisplayed();
        }catch(NoSuchElementException e){
            result = false;
        }catch(StaleElementReferenceException staleException){
            result = false;
        }
        return result;
    }

    public boolean elementIsHidden(WebElement element){
        return !elementIsDisplayed(element);
    }

    private <T> ExpectedCondition<T> displayed(final Function<WebDriver, T> originalFunction){
        return new ExpectedCondition<T>(){
            @Override
            public T apply(WebDriver webdriver){
                try{
                    return originalFunction.apply(webdriver);
                }catch(StaleElementReferenceException sere){
                    throw new NoSuchElementException("Element stale.",sere);
                }
            }
        };
    }

    private <T> ExpectedCondition<T> refreshed(final Function<WebDriver, T> originalFunction){
        return new ExpectedCondition<T>(){
            @Override
            public T apply(WebDriver webdriver){
                try{
                    return originalFunction.apply(webdriver);
                }catch(StaleElementReferenceException sere){
                    throw new NoSuchElementException("Element stale.",sere);
                }
            }
        };
    }
}
