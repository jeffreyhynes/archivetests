package com.creditcards.cccomus.webdriver.pages;

import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreditScoreResultsPage extends AbstractWebPage implements PageObject{
    private final static Logger logger = LoggerFactory.getLogger(CreditScoreResultsPage.class);

    @FindBy(id="credit-score-estimator")
    WebElement creditScoreEstimator;


    @FindBys({@FindBy(id="credit-score-estimator"),@FindBy(tagName = "p")})
    WebElement creditScoreType;

    @FindBys({@FindBy(id="credit-score-estimator"),@FindBy(tagName = "h1")})
    WebElement creditScoreValue;

    @FindBy(className="schumer-box-container")
    WebElement schumerBoxContainer;

    public CreditScoreResultsPage(WebDriverProvider driverProvider){
        super(driverProvider);
    }

    @Override
    public WebElement displayElement(){
        return creditScoreEstimator;
    }

    @Override
    public By displayLocator(){
        return By.id("credit-score-estimator");
    }
}
