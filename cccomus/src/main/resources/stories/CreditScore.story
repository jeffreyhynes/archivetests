
Scenario: View results from Execute CreditScore Estimator

Given I open the website creditcards.com
When I click the Search by Credit Quality link Not Sure
Then the Credit Score Estimator page is displayed

When I select the answer 2 to 4 for question 1
Then question 1.a is displayed

When I select the answer between 6 months and 2 years ago for question 1a
And I select the answer 15 to 20 years ago for question 2
And I select the answer 0 for question 3
And I select the answer more than 15 months ago for question 4
And I select the answer 0 to 4 for question 5
And I select the answer $1000 to $4999 for question 6
And I select the answer I have never missed a payment for question 7
And I select the answer 1 for question 8
And I select the answer Less than $250 for question 8a
And I select the answer 10% to 19% for question 9
And I select the answer more than 7 years ago for question 10
And I click the View Results Button
Then the Please wait page is displayed

When the Please wait page is hidden
Then the Good Estimated Credit Score page is displayed
And the Estimated Credit Score is Good
And the Estimated Credit Score value is from 655-695
And the top ten matching cards displayed are:
|name                                                   |image                                              |
|Capital One® VentureOneSM Rewards Credit Card          |ventureonecardimg.png                              |
|Capital&nbsp;One® Platinum Prestige Credit Card        |capital-one-platinum-prestige-credit-card-5313.png |
|Capital One® Venture<sup>SM</sup> Rewards Credit Card  |capital-one-venture-rewards-credit-card-5313.png   |
|United MileagePlus® Explorer Card                      |united-mileageplus-explorer-card-52213.png         |
|Discover it®                                           |discover-it-4513.png                               |
|Capital One® Card Finder Tool                          |capital-one-card-finder-tool.png                   |
|Discover it®                                           |discover-it-4513.png                               |
|Barclaycard® Rewards MasterCard® - Good Credit         |barclaycard-rewards-mastercard.png                 |
|Chase Freedom<sup>®</sup> Visa                         |freedomvisa.png                                    |
|Chase Sapphire® Card                                   |chase-sapphire.png                                 |

When I click the link Advertising Disclosure
Then the disclosure dialog box is displayed

When I click Close link in the disclosure dialog box
Then the disclosure dialog box is hidden