
Scenario: Verify the Visibility Parameter Converter
Given I am testing
When the test page is displayed
Then pass the test

When the test page is hidden
Then pass the test

Scenario: Verify the Selection Parameter Converter
Given I am testing
When the test element is selected
Then pass the test

When the test element is not selected
Then pass the test
