package com.creditcards.test.steps;

import com.creditcards.pages.PageObject;
import com.creditcards.steps.AbstractSteps;
import com.creditcards.steps.Steps;
import com.creditcards.util.Selection;
import com.creditcards.util.Visibility;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.web.selenium.WebDriverProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

@Steps
public class ParameterConverterSteps extends AbstractSteps{
    private final static Logger logger = LoggerFactory.getLogger(ParameterConverterSteps.class);

    @Autowired
    public ParameterConverterSteps(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    @Given("I am testing")
    public void startTesting(){
        logger.info(" I am testing my stories");
    }

    @When("the test pages is %visibleState")
    public void verifyVisibility(Visibility visibleState){
        logger.info("Visibility Verification");
        switch( visibleState ){
            case DISPLAYED:
                logger.info(" I am DISPLAYED!");
                break;
            case HIDDEN:
                logger.info("Now I am HIDDEN");
                break;
        }
    }

    @When("the test element is %selectedState")
    public void verifySelection(Selection selectedState){
        logger.info("Selection Verification");
        switch( selectedState ){
            case SELECTED:
                logger.info("OK I am SELECTED");
                break;
            case NOT_SELECTED:
                logger.info("I am NOT SELECTED");
                break;
        }
    }

    @Then("pass the test")
    public void passTest(){
        logger.info("The test is over");
    }

    @Override
    public PageObject getPageObject(){
        return null;
    }

    @Override
    public void setWebPageObject(){

    }
}
