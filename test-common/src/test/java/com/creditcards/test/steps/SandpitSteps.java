package com.creditcards.test.steps;

import com.creditcards.steps.Steps;
import org.jbehave.core.annotations.AsParameterConverter;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.web.selenium.WebDriverProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Calendar;

@Steps
public class SandpitSteps{
    private final static Logger logger = LoggerFactory.getLogger(SandpitSteps.class);
    private final WebDriverProvider webDriverProvider;

    @Autowired
    public SandpitSteps(WebDriverProvider webDriverProvider){
        this.webDriverProvider = webDriverProvider;
    }

    @Given("I do nothing")
    public void doNothing(){
        logger.info("@Given I do nothing");
    }

    @Then("I fail")
    public void doFail(){
        logger.info("@Then I FAIL");
    }

    @Then("I pass")
    public void doPass(){
        logger.info("@Then I PASS");
    }

    //-------------------------------------------------

    @Given("the wind blows")
    public void givenWindBlows(){
        logger.info("given the wind blows");
    }

    @When("the wind blows")
    public void whenWindBlows(){
        logger.info("when the wind blows");
    }

    //-------------------------------------------------

    @Given("a plan with calendar date of <date>")
    public void aPlanWithCalendar(@Named("date") Calendar calendar){
        System.out.println(calendar);
    }

    @Then("the claimant should receive an amount of <amount>")
    public void theClaimantReceivesAmount(@Named("amount") double amount){
        System.out.println(amount);
    }

    @AsParameterConverter
    public Calendar calendarDate(String value){
        return (Calendar)new CalendarConverter("dd/MM/yyyy").convertValue(value,Calendar.class);
    }

    //-------------------------------------------------
}
