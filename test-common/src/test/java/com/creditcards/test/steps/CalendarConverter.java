package com.creditcards.test.steps;

import org.apache.commons.lang3.StringUtils;
import org.jbehave.core.steps.ParameterConverters;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class CalendarConverter implements ParameterConverters.ParameterConverter{

    private final SimpleDateFormat dateFormat;

    public CalendarConverter(String dateFormat){
        this.dateFormat = new SimpleDateFormat(dateFormat);
    }

    public boolean accept(Type type){
        if( type instanceof Class<?> ){
            return Calendar.class.isAssignableFrom((Class<?>)type);
        }
        return false;
    }

    public Object convertValue(String value, Type type){
        try{
            if( StringUtils.isBlank(value) || "none".equals(value) ){
                return null;
            }
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateFormat.parse(value));
            return calendar;
        }catch( ParseException e ){
            throw new RuntimeException("Could not convert value " + value + " with format " + dateFormat.toPattern());
        }
    }
}

