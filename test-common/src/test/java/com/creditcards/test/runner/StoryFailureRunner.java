package com.creditcards.test.runner;

import org.jbehave.core.annotations.AfterStories;
import org.jbehave.core.junit.spring.SpringAnnotatedEmbedderRunner;
import org.junit.runner.RunWith;

@RunWith(SpringAnnotatedEmbedderRunner.class)
public class StoryFailureRunner extends TraderRunner{

    @AfterStories
    public void afterStories(){
        throw new RuntimeException("Bum went the stories");
    }
}
