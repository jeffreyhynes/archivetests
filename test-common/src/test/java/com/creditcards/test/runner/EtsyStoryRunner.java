package com.creditcards.test.runner;

import org.jbehave.core.InjectableEmbedder;
import org.jbehave.core.annotations.Configure;
import org.jbehave.core.annotations.UsingEmbedder;
import org.jbehave.core.annotations.spring.UsingSpring;
import org.jbehave.core.embedder.Embedder;
import org.jbehave.core.junit.spring.SpringAnnotatedEmbedderRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

@RunWith(SpringAnnotatedEmbedderRunner.class)

@Configure()
@UsingEmbedder(embedder = Embedder.class,generateViewAfterStories = true,ignoreFailureInStories = true,
               ignoreFailureInView = true)
@UsingSpring(resources = { "jbehave-test-configuration.xml","selenium-test.xml" })
public class EtsyStoryRunner extends InjectableEmbedder{
    private final TestStoryFinder storyFinder = new TestStoryFinder();

    @Test
    public void run(){
        injectedEmbedder().runStoriesAsPaths(storyFinder.storyPaths(getIncludes()));
    }

    private List<String> getIncludes(){
        return newArrayList("**/*.story");
    }
}
