package com.creditcards.test.runner;

import org.jbehave.core.Embeddable;

/**
 * Copyright (c) 2013-05-28, CreditCards.com. All Rights Reserved.
 * User: jeffreyhynes
 * Date: 2013-05-28
 * com.creditcards.runner
 */
public interface StoryPathResolver{
    String resolve(Class<? extends Embeddable> embeddableClass);
}
