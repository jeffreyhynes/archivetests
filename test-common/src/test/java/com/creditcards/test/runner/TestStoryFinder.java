package com.creditcards.test.runner;

import org.jbehave.core.io.StoryFinder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.jbehave.core.io.CodeLocations.codeLocationFromPath;

public class TestStoryFinder{
    private final static Logger logger = LoggerFactory.getLogger(TestStoryFinder.class);
    private static final String STORY_DIRECTORY = "stories/";

    public List<String> storyPaths(List<String> includes){
        String searchInDirectory = codeLocationFromPath("stories/").getFile();
        logger.info(" ********** STORY PATHS" + searchInDirectory);
        return new StoryFinder().findPaths(searchInDirectory,includes,getExcludes(),"stories/");
    }

    public List<String> storyPaths(List<String> includes, List<String> excludes){
        String currentDirectory = new File("").toURI().getPath();
        String storyDirectory = currentDirectory.endsWith("classes/") ? "stories/" : "target/classes/stories/";

        String searchInDirectory = codeLocationFromPath(storyDirectory).getFile();
        logger.info("   ** Story Search Directory:" + searchInDirectory);
        return new StoryFinder().findPaths(searchInDirectory,includes,excludes,TestStoryFinder.STORY_DIRECTORY);
    }

    List<String> getExcludes(){
        return new ArrayList<String>();
    }
}
