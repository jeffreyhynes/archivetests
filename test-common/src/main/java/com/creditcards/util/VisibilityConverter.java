package com.creditcards.util;

import org.jbehave.core.steps.ParameterConverters.EnumConverter;
import org.jbehave.core.steps.ParameterConverters.ParameterConverter;

import java.lang.reflect.Type;

public class VisibilityConverter extends EnumConverter implements ParameterConverter{
    @Override
    public boolean accept(Type type){
        return type instanceof Class<?> && Visibility.class.isAssignableFrom((Class<?>)type);
    }

    @Override
    public Object convertValue(String visibleState, Type type){
        return Visibility.valueOfByState(visibleState);
    }
}
