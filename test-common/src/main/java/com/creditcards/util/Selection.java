package com.creditcards.util;

import org.apache.commons.lang3.StringUtils;

public enum Selection{
    SELECTED("selected",true),
    NOT_SELECTED("not selected",false);

    private final boolean isSelected;
    private final String state;

    Selection(String name, boolean selected){
        this.isSelected = selected;
        this.state = name;
    }

    public boolean isSelected(){
        return this.isSelected;
    }

    public static Selection valueOfByState(String selection){
        Selection result = null;
        for( Selection component : Selection.values() ){
            if( StringUtils.equalsIgnoreCase(selection,component.state) ){
                result = component;
                break;
            }
        }
        return result;
    }

    public boolean equalsValue(boolean isSelected){
        return this.isSelected == isSelected;
    }

}
