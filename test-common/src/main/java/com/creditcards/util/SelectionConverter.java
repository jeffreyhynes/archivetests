package com.creditcards.util;

import org.jbehave.core.steps.ParameterConverters.EnumConverter;

import java.lang.reflect.Type;

public class SelectionConverter extends EnumConverter{
    @Override
    public boolean accept(Type type){
        return type instanceof Class<?> && Selection.class.isAssignableFrom(( Class<?> )type);
    }

    @Override
    public Object convertValue(String value,Type type){
        return Selection.valueOfByState(value);
    }
}
