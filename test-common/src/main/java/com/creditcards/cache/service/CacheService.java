package com.creditcards.cache.service;

import java.util.concurrent.ConcurrentHashMap;

/**
 * <code>CacheService</code> implementations manages the caching for the
 * <code>SharedContext</code>.
 * <p/>
 * Copyright (c) 2012-12, CreditCards.com. All Rights Reserved.
 *
 * @author Jeffrey Hynes
 */
public interface CacheService{
    void setSharedContext(ConcurrentHashMap<Object, Object> sharedContext);

    boolean containsKey(Object key);

    void put(Object key, Object value);

    Object get(Object key);
}