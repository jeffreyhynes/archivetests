package com.creditcards.cache.service;

import com.creditcards.util.SharedContext;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Implementation of {@link CacheService} that provides access
 * to the SharedContext Cache Map
 * <p/>
 * Copyright (c) 2012-12, CreditCards.com. All Rights Reserved.
 *
 * @author Jeffrey Hynes
 */
public class CacheServiceImpl implements CacheService{

    //Shared Context is injected...
    private ConcurrentHashMap<Object, Object> sharedContext;


    /**
     * Stores the shared context mapping {@link SharedContext}.
     *
     * @param sharedContext the cached Map {@link SharedContext}.
     */
    @Override
    public void setSharedContext(ConcurrentHashMap<Object, Object> sharedContext){
        this.sharedContext = sharedContext;
    }

    @Override
    public void put(Object key, Object value) {
        sharedContext.put(key,value);
    }

    @Override
    public Object get(Object key) {
        return this.sharedContext.get(key);
    }

    /**
     * Returns true if the object is stored in the existing cache.
     *
     * @param key possible key
     * @return <tt>true</tt> if and only if the specified object
     *         is a key that is being stached, as determined by the
     *         <tt>equals</tt> method; <tt>false</tt> otherwise.
     * @throws NullPointerException if the specified key is null
     */
    @SuppressWarnings("unchecked")
    @Override
    public boolean containsKey(Object key){
        return sharedContext.containsKey(key);
    }
}