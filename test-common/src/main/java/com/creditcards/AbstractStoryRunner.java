package com.creditcards;

import org.jbehave.core.annotations.Configure;
import org.jbehave.core.annotations.UsingEmbedder;
import org.jbehave.core.annotations.spring.UsingSpring;
import org.jbehave.core.embedder.Embedder;
import org.jbehave.core.junit.JUnitStories;
import org.jbehave.core.steps.ParameterConverters;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@Configure(parameterConverters = ParameterConverters.EnumConverter.class)
@UsingEmbedder(embedder = Embedder.class,
               generateViewAfterStories = true,
               ignoreFailureInStories = true,
               ignoreFailureInView = true,
               verboseFailures = true)
@UsingSpring(resources = { "jbehave-spring-configuration.xml","application-configuration.xml" })
public abstract class AbstractStoryRunner extends JUnitStories{
    protected abstract List<String> storyPaths();
}
