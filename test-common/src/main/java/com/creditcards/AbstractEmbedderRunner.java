package com.creditcards;

import org.jbehave.core.InjectableEmbedder;
import org.jbehave.core.annotations.Configure;
import org.jbehave.core.annotations.UsingEmbedder;
import org.jbehave.core.annotations.spring.UsingSpring;
import org.jbehave.core.embedder.Embedder;
import org.jbehave.core.failures.FailingUponPendingStep;
import org.jbehave.core.junit.spring.SpringAnnotatedEmbedderRunner;
import org.jbehave.web.selenium.SeleniumConfiguration;
import org.junit.runner.RunWith;

import java.util.List;

@RunWith(SpringAnnotatedEmbedderRunner.class)
@Configure(using = SeleniumConfiguration.class,pendingStepStrategy = FailingUponPendingStep.class)
@UsingEmbedder(embedder = Embedder.class,generateViewAfterStories = true,ignoreFailureInStories = true,
               ignoreFailureInView = false,storyTimeoutInSecs = 100,threads = 1,metaFilters = "-skip")
@UsingSpring(resources = { "jbehave-spring-configuration.xml","application-configuration.xml" })
public abstract class AbstractEmbedderRunner extends InjectableEmbedder{
    protected abstract List<String> getIncludes();
    protected abstract List<String> getExcludes();
}
