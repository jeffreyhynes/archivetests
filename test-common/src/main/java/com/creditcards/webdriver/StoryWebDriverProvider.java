package com.creditcards.webdriver;

import org.jbehave.web.selenium.DelegatingWebDriverProvider;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import static com.creditcards.webdriver.StoryWebDriverProvider.Browser.valueOf;
import static org.apache.commons.lang3.StringUtils.upperCase;

public class StoryWebDriverProvider extends DelegatingWebDriverProvider{
    private static final Logger logger = LoggerFactory.getLogger(StoryWebDriverProvider.class);

    @Value("#{settings['browser']}")
    private String browser;

    @Value("#{settings['profileName']}")
    private String profileNameProperty;

    @Override
    public void initialize(){
        logger.debug("Loading the Webdriver for the following browser:" + browser);
        WebDriver webDriver = valueOf(upperCase(browser)).createWebDriver(this);
        delegate.set(webDriver);
    }

    public WebDriver createWebDriver(){
        return StoryWebDriverProvider.Browser.valueOf(upperCase(browser)).createWebDriver(this);
    }

    protected enum Browser{
        FIREFOX{
            @Override
            WebDriver createWebDriver(StoryWebDriverProvider webDriverProvider){
                return webDriverProvider.createFirefoxDriver();
            }
        },
        FIREFOX_PROFILE{
            WebDriver createWebDriver(StoryWebDriverProvider webDriverProvider){
                return webDriverProvider.createFirefoxDriverWithProfile();
            }
        },
        IE{
            WebDriver createWebDriver(StoryWebDriverProvider webDriverProvider){
                return webDriverProvider.createInternetExplorerDriver();
            }
        },
        CHROME{
            WebDriver createWebDriver(StoryWebDriverProvider webDriverProvider){
                return webDriverProvider.createChromeDriver();
            }
        },
        SAFARI{
            WebDriver createWebDriver(StoryWebDriverProvider webDriverProvider){
                return webDriverProvider.createSafariDriver();
            }
        };

        abstract WebDriver createWebDriver(StoryWebDriverProvider sf);
    }

    private WebDriver createFirefoxDriverWithProfile(){
        ProfilesIni profileList = new ProfilesIni();
        FirefoxProfile profile = profileList.getProfile(profileNameProperty);
        return new FirefoxDriver(profile);
    }

    private WebDriver createFirefoxDriver(){
        return new FirefoxDriver();
    }

    private WebDriver createInternetExplorerDriver(){
        return new InternetExplorerDriver();
    }

    private WebDriver createChromeDriver(){
        return new ChromeDriver();
    }

    private WebDriver createSafariDriver(){
        return new SafariDriver();
    }
}
