package com.creditcards.pages;

import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class MenuBarHoverIconPageObject extends AbstractPageObject implements MenuBarHoverIcon{

    protected MenuBarHoverIconPageObject(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    @Override
    public boolean isOpen(){
        return elementIsDisplayed(hoverContent());
    }

    @Override
    public boolean isClosed(){
        return elementIsHidden(displayElement(),By.id("summary"));
    }

    @Override
    public void click(){
        displayElement().click();
    }

    @Override
    public void hover(){
        Actions builder = new Actions(getDriverProvider().get());
        builder.moveToElement(displayElement());
        builder.build().perform();

        new WebDriverWait(getDriverProvider().get(),10).until(ExpectedConditions.visibilityOf(hoverContent()));
    }
}