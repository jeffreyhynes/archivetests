package com.creditcards.pages;

import com.creditcards.util.Visibility;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public interface PageObject{

    void initElements();

    void waitUntilLoaded();

    By displayLocator();

    WebElement displayElement();

    boolean visibleStateMatches(Visibility visibility);

    boolean isDisplayed();

    boolean isHidden();

    boolean elementExists(By by);

    boolean elementExists(WebElement element, By by);

    boolean elementIsDisplayed(WebElement element);

    boolean elementIsDisplayed(WebElement element, By by);

    boolean elementIsHidden(WebElement element, By by);

    boolean elementIsHidden(WebElement element);

    void waitToDisplay();

    void waitToClose();

    void waitUntilUnLoaded();

    void waitAndClick();

    void waitExpectedSelectionClassAttributeToBe(final WebElement element, final String attribute);

    void waitElementToDisplay(final WebElement element);

    void waitElementToDisplay(final WebElement parent, final By target);
}