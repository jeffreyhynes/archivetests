package com.creditcards.pages;

import org.openqa.selenium.WebElement;

public interface MenuBarHoverIcon extends MenuBarIcon{
    void hover();

    WebElement hoverContent();

    boolean isOpen();

    boolean isClosed();
}
