package com.creditcards.pages;

import org.openqa.selenium.By;

public interface NavigationTab{
    public String getDisplayName();

    public String getId();

    public String getHref();

    public By byIdSelector();
}