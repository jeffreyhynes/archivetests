package com.creditcards.pages;

import org.jbehave.web.selenium.WebDriverProvider;

public abstract class MenuBarIconPageObject extends AbstractPageObject implements MenuBarIcon{

    protected MenuBarIconPageObject(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    @Override
    public void click(){
        displayElement().click();
    }
}
