package com.creditcards.pages;

import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;

public abstract class NavigationMenuPageObject extends AbstractPageObject implements NavigationMenu{

    public NavigationMenuPageObject(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    @Override
    public final boolean isTabSelected(NavigationTab navigationTab){
        return navigationTab.equals(getSelectedNavigationTab());
    }

    @Override
    public void clickTab(NavigationTab navigationTab){
        navigationMenu().findElement(navigationTab.byIdSelector()).findElement(By.tagName("a")).click();
    }
}