package com.creditcards.pages;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public abstract class SelectPageObject extends AbstractPageObject implements Select{
    public enum SelectAction{
        MENU("ul"),
        OPTIONS("li");

        private final By tag;

        SelectAction(String tag){
            this.tag = By.tagName(tag);
        }

        public By byTagName(){
            return tag;
        }
    }

    @Autowired
    public SelectPageObject(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    public void click(){
        menuSelection().click();
        new WebDriverWait(getDriverProvider().get(),10).until(ExpectedConditions.visibilityOf(
                dropdownContainer().findElement(SelectAction.OPTIONS.byTagName())));
    }

    public WebElement dropdownContent(){
        return dropdownContainer().findElement(SelectAction.OPTIONS.byTagName());
    }

    public boolean isClosed(){
        return elementIsHidden(dropdownContainer(),SelectAction.MENU.byTagName());
    }

    public boolean isOpen(){
        return elementIsDisplayed(dropdownContainer(),SelectAction.MENU.byTagName());
    }

    public void selectOption(String text){
        Actions builder = new Actions(getDriverProvider().get());
        WebElement menuOption = menuObject().findElement(By.linkText(text));
        builder.moveToElement(menuObject());
        builder.moveToElement(menuOption);
        builder.click(menuOption);
        builder.build().perform();

        new WebDriverWait(getDriverProvider().get(),10)
                .until(ExpectedConditions.visibilityOf(dropdownContainer().findElement(SelectAction.MENU.byTagName())));
    }

    public void hoverOption(String text){
        Actions builder = new Actions(getDriverProvider().get());
        WebElement menuOption = menuObject().findElement(By.linkText(text));
        builder.moveToElement(menuObject());
        builder.moveToElement(menuOption);
        builder.click(menuOption);
        builder.build().perform();

        new WebDriverWait(getDriverProvider().get(),10)
                .until(ExpectedConditions.visibilityOf(dropdownContainer().findElement(SelectAction.MENU.byTagName())));
    }

    public List<String> getMenuOptions(){
        List<String> fundingList = new ArrayList<String>(menuOptions().size());
        for( WebElement option : menuOptions() ){
            fundingList.add(option.getText());
        }
        return fundingList;
    }

    public boolean equals(List<String> expectedList){
        return CollectionUtils.isEqualCollection(getMenuOptions(),expectedList);
    }

    public void selectOption(MenuOption menuOption){
        selectOption(menuOption.displayText());
    }

    public boolean isSelectedEqual(String expectedText){
        return StringUtils.equalsIgnoreCase(expectedText,menuSelection().getText());
    }
}
