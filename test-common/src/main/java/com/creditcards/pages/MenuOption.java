package com.creditcards.pages;

import org.openqa.selenium.By;

public interface MenuOption{
    String displayText();

    By byLinkText();

    int index();
}
