package com.creditcards.pages;

public interface ContainerPageObject extends PageObject{
    void initPageElements();
}
