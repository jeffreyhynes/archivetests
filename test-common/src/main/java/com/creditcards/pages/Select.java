package com.creditcards.pages;

import org.openqa.selenium.WebElement;

import java.util.List;

public interface Select{
    WebElement dropdownContainer();

    WebElement dropdownContent();

    WebElement menuObject();

    List<WebElement> menuOptions();

    List<String> getMenuOptions();

    WebElement menuSelection();

    boolean equals(List<String> expectedList);

    void selectOption(String text);

    void selectOption(MenuOption menuOption);

    void hoverOption(String text);

    void click();

    boolean isOpen();

    boolean isClosed();

    boolean isSelectedEqual(String expectedText);
}