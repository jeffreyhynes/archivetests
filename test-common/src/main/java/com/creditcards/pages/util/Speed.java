package com.creditcards.pages.util;

public enum Speed{
    SLOW("1000"),
    MEDIUM("500"),
    FAST("0");
    private final String timeOut;

    private Speed(String timeOut){
        this.timeOut = timeOut;
    }

    public String getTimeOut(){
        return timeOut;
    }
}
