package com.creditcards.pages.util;

public interface ActionableWebElement{

    public void invoke();
}
