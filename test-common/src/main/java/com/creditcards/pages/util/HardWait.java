package com.creditcards.pages.util;

import static java.lang.Thread.sleep;

public class HardWait{
    public void waitForAnimation(){
        waitForMilliSeconds(500);
    }

    public void waitForMilliSeconds(long millisecs){
        try{
            sleep(millisecs);
        }catch( InterruptedException e ){
            e.printStackTrace();
        }
    }
}
