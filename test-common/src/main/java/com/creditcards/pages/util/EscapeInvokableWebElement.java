package com.creditcards.pages.util;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class EscapeInvokableWebElement implements ActionableWebElement{
    private final WebElement invokableWebElement;

    public EscapeInvokableWebElement(WebElement invokableWebElement){
        this.invokableWebElement = invokableWebElement;
    }

    public void invoke(){
        invokableWebElement.sendKeys(Keys.ESCAPE);
    }
}
