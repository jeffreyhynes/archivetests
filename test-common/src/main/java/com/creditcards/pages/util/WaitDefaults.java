package com.creditcards.pages.util;

public enum WaitDefaults{
    DEFAULT_RETRIES(Long.MAX_VALUE),
    DEFAULT_TIMEOUT(30000L),
    DEFAULT_INTERVAL(100);

    private final long value;

    WaitDefaults(long value){
        this.value = value;
    }

    public long value(){
        return this.value;
    }
}
