package com.creditcards.pages.util;

import org.openqa.selenium.WebElement;

import static org.openqa.selenium.Keys.ENTER;

public class EnterInvokableWebElement implements ActionableWebElement{
    private final WebElement invokableWebElement;

    public EnterInvokableWebElement(WebElement invokableWebElement){
        this.invokableWebElement = invokableWebElement;
    }

    public void invoke(){
        invokableWebElement.sendKeys(ENTER);
    }
}
