package com.creditcards.pages.util;

class GaveUpWaitingException extends RuntimeException{

    private static final long serialVersionUID = 0L;

    public GaveUpWaitingException(String timeoutMessage){
        super(timeoutMessage);
    }
}