package com.creditcards.pages.util;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

// Makes sending the "Enter" key work on all operating systems.
public class SendEnterKey{

    private static final CharSequence ENTER = enterKeyForOperatingSystem(System.getProperty("os.name"));

    private static CharSequence enterKeyForOperatingSystem(String name){
        CharSequence key;
        if( name.startsWith("Linux") ){
            key = Keys.RETURN;
        }else if( name.startsWith("Mac OS") ){
            key = Keys.RETURN;
        }else{
            key = Keys.ENTER;
        }
        return key;
    }

    public static void toElement(WebElement element){
        element.sendKeys(ENTER);
    }
}
