package com.creditcards.pages;

import com.creditcards.util.Visibility;
import com.google.common.base.Function;
import org.apache.commons.lang3.StringUtils;
import org.jbehave.web.selenium.WebDriverPage;
import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractPageObject extends WebDriverPage implements PageObject{
    private final static Logger logger = LoggerFactory.getLogger(AbstractPageObject.class);

    public AbstractPageObject(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    @Override
    public void initElements(){
        int DRIVER_WAIT = 5;
        PageFactory.initElements(new AjaxElementLocatorFactory(getDriverProvider().get(),DRIVER_WAIT),this);
    }

    public boolean isDisplayed(){
        WebElement element = displayElement();
        return null != element && element.isDisplayed();
    }

    public boolean isHidden(){
        return null == displayElement() || !displayElement().isDisplayed();
    }

    public boolean elementExists(By by){
        boolean result = true;
        try{
            getDriverProvider().get().findElement(by);
        }catch( NoSuchElementException e ){
            result = false;
        }
        return result;
    }

    public boolean elementExists(WebElement element, By by){
        try{
            element.findElement(by);
        }catch( NoSuchElementException e ){
            return false;
        }catch( StaleElementReferenceException staleException ){
            return false;
        }
        return true;
    }

    public boolean elementIsDisplayed(WebElement element){
        boolean result;
        try{
            result = null != element && element.isDisplayed();
        }catch( NoSuchElementException e ){
            result = false;
        }catch( StaleElementReferenceException staleException ){
            result = false;
        }
        return result;
    }

    public boolean elementIsDisplayed(WebElement element, By by){
        if( !elementExists(element,by) ){
            return false;
        }
        WebElement innerElement = element.findElement(by);
        return innerElement.isDisplayed() && StringUtils.isNotBlank(innerElement.getText());
    }

    public boolean elementIsHidden(WebElement element, By by){
        return !( elementIsDisplayed(element,by) );
    }

    public boolean elementIsHidden(WebElement element){
        return !( elementIsDisplayed(element) );
    }

    public void waitUntilLoaded(){
        initElements();
        new WebDriverWait(getDriverProvider().get(),30)
                .until(refreshed(ExpectedConditions.visibilityOf(displayElement())));
    }

    public void waitToDisplay(){
        new WebDriverWait(getDriverProvider().get(),30)
                .until(refreshed(ExpectedConditions.visibilityOf(displayElement())));
    }

    public void waitToClose(){
        new WebDriverWait(getDriverProvider().get(),30)
                .until(displayed(ExpectedConditions.invisibilityOfElementLocated(displayLocator())));
    }

    public void waitUntilUnLoaded(){
        new WebDriverWait(getDriverProvider().get(),30)
                .until(displayed(ExpectedConditions.invisibilityOfElementLocated(displayLocator())));
    }

    public void waitAndClick(){
        int DRIVER_WAIT = 5;
        PageFactory.initElements(new AjaxElementLocatorFactory(getDriverProvider().get(),DRIVER_WAIT),this);
    }

    public void waitExpectedSelectionClassAttributeToBe(final WebElement element, final String attribute){
        new WebDriverWait(getDriverProvider().get(),10).until(elementSelectionClassAttributeToBe(element,attribute));
    }

    public void waitElementToDisplay(final WebElement element){
        new WebDriverWait(getDriverProvider().get(),30).until(refreshed(ExpectedConditions.visibilityOf(element)));
    }

    public void waitElementToDisplay(final WebElement parent, final By target){
        new WebDriverWait(getDriverProvider().get(),30).until(elementToBeDisplayed(parent,target));
    }

    public boolean visibleStateMatches(Visibility visibility){
        return visibility.isDisplayed() ? elementIsDisplayed(displayElement()) : elementIsHidden(displayElement());
    }

    private <T> ExpectedCondition<T> refreshed(final Function<WebDriver, T> originalFunction){
        return new ExpectedCondition<T>(){
            @Override
            public T apply(WebDriver webdriver){
                try{
                    return originalFunction.apply(webdriver);
                }catch( StaleElementReferenceException sere ){
                    throw new NoSuchElementException("Element stale.",sere);
                }
            }
        };
    }

    private <T> ExpectedCondition<T> displayed(final Function<WebDriver, T> originalFunction){
        return new ExpectedCondition<T>(){
            @Override
            public T apply(WebDriver webdriver){
                try{
                    return originalFunction.apply(webdriver);
                }catch( StaleElementReferenceException sere ){
                    throw new NoSuchElementException("Element stale.",sere);
                }
            }
        };
    }

    private ExpectedCondition<WebElement> elementToBeDisplayed(final WebElement parent, final By by){
        return new ExpectedCondition<WebElement>(){
            @Override
            public WebElement apply(WebDriver driver){
                WebElement element = parent.findElement(by);
                return element.isDisplayed() ? element : null;
            }
        };
    }

    private ExpectedCondition<WebElement> elementSelectionClassAttributeToBe(final WebElement element,
                                                                             final String attribute){
        return new ExpectedCondition<WebElement>(){
            @Override
            public WebElement apply(WebDriver driver){
                return StringUtils.containsIgnoreCase(element.getAttribute("class"),attribute) ? element : null;
            }
        };
    }
}