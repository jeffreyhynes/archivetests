package com.creditcards.pages;

public interface ClickableIcon{
    void click();
}
