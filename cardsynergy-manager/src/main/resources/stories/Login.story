
Scenario: Successfully log into CardSynergy Manager

Given I am an Affiliate
When I open CardSynergy Manager
Then the Login page is displayed

When I enter my username <username>
And I enter my password <password>
And I click the Login button
Then the Overview page is displayed

Examples:
|username   |password   |
|superman   |cards      |