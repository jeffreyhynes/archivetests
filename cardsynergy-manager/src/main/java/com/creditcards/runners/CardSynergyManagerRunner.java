package com.creditcards.runners;

import com.creditcards.AbstractEmbedderRunner;
import com.creditcards.io.StoryLoader;
import com.creditcards.webdriver.StoryWebDriverProvider;
import org.jbehave.core.annotations.spring.UsingSpring;
import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.embedder.StoryControls;
import org.jbehave.core.failures.FailingUponPendingStep;
import org.jbehave.core.io.LoadFromClasspath;
import org.jbehave.web.selenium.SeleniumConfiguration;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

@UsingSpring(resources = {"application-configuration.xml","selenium.xml"})
public class CardSynergyManagerRunner extends AbstractEmbedderRunner{
    private final static Logger logger = LoggerFactory.getLogger(CardSynergyManagerRunner.class);
    private final StoryLoader storyLoader = new StoryLoader();

    @Autowired
    private StoryWebDriverProvider webDriverProvider;

    @Test
    public void run() throws Throwable{
        logger.info("****************** Running the CardSynergy Manager Story Runner");
        Configuration configuration = injectedEmbedder().configuration();
        configuration.useFailureStrategy(new FailingUponPendingStep())
                     .useStoryControls(new StoryControls().doResetStateBeforeScenario(false))
                     .useStoryLoader(new LoadFromClasspath(CardSynergyManagerRunner.class));
        SeleniumConfiguration seleniumConfiguration = (SeleniumConfiguration)configuration;
        seleniumConfiguration.useWebDriverProvider(webDriverProvider);
        List<String> storiesAsPath=storyLoader.storyPaths(storyPaths(),getExcludes());
        logger.info("Embedder Story Path:" + storiesAsPath);
        injectedEmbedder().runStoriesAsPaths(storiesAsPath);
    }

    protected List<String> storyPaths(){
        return newArrayList("**/*.story");
    }

    protected List<String> getIncludes(){
        return newArrayList("**/test.story");
        //return newArrayList("**/ReportDashboard.story");
        //return newArrayList("**/Login.story");
    }

    protected List<String> getExcludes(){
        return null;
    }
}
