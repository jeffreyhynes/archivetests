package com.creditcards.cardsynergy.steps;

import com.creditcards.cardsynergy.utils.Users;
import com.creditcards.steps.Steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.web.selenium.WebDriverProvider;
import org.springframework.beans.factory.annotation.Autowired;

@Steps
public class UserSteps{

    private final WebDriverProvider webDriverProvider;

    @Autowired
    public UserSteps(WebDriverProvider webDriverProvider){
        this.webDriverProvider = webDriverProvider;
    }

    @Given("I am an Affiliate")
    public void defineUser() {
       Users user = Users.valueOfByName("Affiliate");
    }
}
