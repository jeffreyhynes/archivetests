package com.creditcards.cardsynergy.steps;

import com.creditcards.cardsynergy.pages.LoginPage;
import com.creditcards.steps.Steps;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.web.selenium.WebDriverProvider;
import org.springframework.beans.factory.annotation.Autowired;

import static org.testng.Assert.assertTrue;

@Steps
public class LoginSteps{
    private final WebDriverProvider webDriverProvider;

    @Autowired
    LoginPage loginPage;

    @Autowired
    public LoginSteps(WebDriverProvider webDriverProvider){
        this.webDriverProvider = webDriverProvider;
    }

    @When("I open CardSynergy Manager")
    public void openApplication(){
        loginPage.go();
        loginPage.waitUntilLoaded();
    }

    @Then("the Login pages is displayed")
    public void loginPageDisplayed(){
        assertTrue(loginPage.isDisplayed());
    }
    @When("I enter my username <username>")
    public void enterUserName(@Named("username") String username) {
        loginPage.enterUsername(username);
    }

    @When("I enter my password <password>")
    public void enterPassword(@Named("password") String password) {
        loginPage.enterPassword(password);
    }

    @When("I click the Login button")
    public void clickLogin() {
        loginPage.clickLogin();
    }
}
