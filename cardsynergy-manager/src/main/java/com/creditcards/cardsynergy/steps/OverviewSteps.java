package com.creditcards.cardsynergy.steps;

import com.creditcards.cardsynergy.pages.OverviewPage;
import com.creditcards.steps.Steps;
import org.jbehave.core.annotations.Then;
import org.jbehave.web.selenium.WebDriverProvider;
import org.springframework.beans.factory.annotation.Autowired;

@Steps
public class OverviewSteps{
    private final WebDriverProvider webDriverProvider;

    @Autowired
    OverviewPage overviewPage;

    @Autowired
    public OverviewSteps(WebDriverProvider webDriverProvider){
        this.webDriverProvider = webDriverProvider;
    }

    @Then("the Overview pages is displayed")
    public void pageDisplayed(){
        overviewPage.isDisplayed();
    }
}
