package com.creditcards.cardsynergy.pages;

import com.creditcards.pages.AbstractPageObject;
import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class LoginPage extends AbstractPageObject{

    private enum PageElements{
        TITLE("CardSynergy Manager"),
        PARENT("login-wrapper"),
        FORM_ID("login"),
        USERNAME("username"),
        PASSWORD("password"),
        URL("login.php");

        private final String value;

        PageElements(String element){
            this.value = element;
        }

        public By byIDSelector() {
            return By.id(value);
        }

        public String toString(){
            return value;
        }
    }

    @Value("#{settings['applicationUrl']}")
    private String applicationUrl;

    @FindBy(id = "login-wrapper")
    @CacheLookup
    private WebElement parentContainer;

    @FindBy(id = "username")
    @CacheLookup
    private WebElement loginUsername;

    @FindBy(id = "password")
    @CacheLookup
    private WebElement loginPassword;

    @FindBys({@FindBy(id = "login"), @FindBy(css = "input[name='submit_login']")})
    private WebElement loginButton;

    @Autowired
    public LoginPage(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    public void go() {
        StringBuilder builder = new StringBuilder();
        builder.append("http://").append(applicationUrl);
        builder.append("/").append(PageElements.URL.toString());
        get(builder.toString());
    }

    @Override
    public WebElement displayElement(){
        return parentContainer;
    }

    @Override
    public By displayLocator(){
        return PageElements.PARENT.byIDSelector();
    }

    public void enterUsername(String email) {
        loginUsername.clear();
        loginUsername.sendKeys(email);
    }

    public void enterPassword(String password) {
        loginPassword.clear();
        loginPassword.sendKeys(password);
    }
    public void clickLogin() {
        loginButton.click();
    }

}
