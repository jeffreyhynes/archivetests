package com.creditcards.cardsynergy.pages;

import com.creditcards.pages.AbstractPageObject;
import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OverviewPage  extends AbstractPageObject{
    private enum PageElements{
        TITLE("subhead"),
        URL("index.php"),
        WELCOME("welcome");

        private final String value;

        PageElements(String element){
            this.value = element;
        }

        public By byIDSelector() {
            return By.id(value);
        }

        public String toString(){
            return value;
        }
    }

    @FindBys({@FindBy(id = "subhead"), @FindBy(tagName = "h1")})
    private WebElement dashboardHeader;

    @Autowired
    public OverviewPage(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    @Override
    public By displayLocator(){
        return PageElements.TITLE.byIDSelector();
    }

    @Override
    public WebElement displayElement(){
        return dashboardHeader;
    }
}
