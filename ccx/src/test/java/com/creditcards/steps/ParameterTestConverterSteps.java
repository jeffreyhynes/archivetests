package com.creditcards.steps;

import com.creditcards.ccx.pages.application.ApplicationPages;
import com.creditcards.ccx.pages.content.ContentPageObjects;
import com.creditcards.ccx.pages.converters.ApplicationPageConverter;
import com.creditcards.ccx.pages.converters.ContentPageObjectConverter;
import com.creditcards.ccx.pages.converters.CurrentMonthConverter;
import com.creditcards.ccx.pages.converters.DashBoardReportsConverter;
import com.creditcards.ccx.pages.converters.EpochConverter;
import com.creditcards.ccx.pages.converters.PerformanceMetricsConverter;
import com.creditcards.ccx.pages.converters.PerformanceReportPageConverter;
import com.creditcards.ccx.pages.header.HeaderPageObjects;
import com.creditcards.ccx.pages.reporting.DashboardReportPageObject.DashboardReports;
import com.creditcards.ccx.pages.reporting.PerformanceBarPageObject.PerformanceMetrics;
import com.creditcards.ccx.pages.reporting.PerformanceReports;
import com.creditcards.ccx.pages.subHeader.SubHeaderPageObjects;
import com.creditcards.pages.PageObject;
import com.creditcards.util.Visibility;
import org.jbehave.core.annotations.AsParameterConverter;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;
import org.jbehave.core.steps.ParameterConverters;
import org.jbehave.core.steps.ParameterConverters.EnumConverter;
import org.jbehave.web.selenium.WebDriverProvider;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.Type;

@Steps
public class ParameterTestConverterSteps extends AbstractSteps{
    private final static Logger logger = LoggerFactory.getLogger(ParameterTestConverterSteps.class);

    @Autowired
    public ParameterTestConverterSteps(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    @Given( "I am testing" )
    public void startTesting(){
        logger.info("------ START Testing ");
    }

    @When( "the title displays the %current_month" )
    public void verifyCurrentMonthParameter(DateTime.Property current_month){
        logger.info("Verifying the current month parameter");
        logger.info("Entered date = " + current_month.getAsShortText());
    }

    @Then( "pass current month parameter" )
    public void passCurrentMonth(){
        logger.info("Current Month is GOOD");
    }

    @When( "I am interested in the %dashboardReport Central report" )
    public void verifyReportConverter(DashboardReports dashboardReport){
        logger.info("Verifying the Selected Dashboard Report Converter");
        logger.info("Selected report:" + dashboardReport.getName());
    }

    @Then( "pass the %dashboardReport Central report" )
    public void passReport(DashboardReports dashboardReport){
        logger.info("Report:" + dashboardReport.getName() + " PASSED!");
    }

    @Then( "pass the Clicks report" )
    public void passDashboardReports(){
        logger.info("Dashboard ReportConverter Is DONE");
    }

    @When( "I am verifying the %performanceMetric performance metric" )
    public void verifyPerformanceMetric(PerformanceMetrics performanceMetric){
        logger.info("Verifying the Performance Metric Converter");
        logger.info("Metric:" + performanceMetric.getDisplay());
    }

    @Then( "pass the %performanceMetric performance metric check" )
    public void passPerformanceMetric(PerformanceMetrics performanceMetric){
        logger.info("Metric:" + performanceMetric.getDisplay() + " PASSED!");
    }

    @When( "I have a epoch date of %epochDate" )
    public void convertEpoch(DateTime epochDate){
        logger.info("My date" + epochDate);
    }

    @When( "I am verifying the %applicationPages pages is %visibleState" )
    public void visibleState(ApplicationPages applicationPages, Visibility visibleState){
        logger.info("Application pages:" + applicationPages.getName());
    }

    @Then( "pass the %applicationPages pages check" )
    public void passReport(ApplicationPages applicationPages){
        logger.info("Application pages CHECK - PASS");
    }

    @Then( "pass the %contentPageObject content check" )
    public void passReport(ContentPageObjects contentPageObject){
        logger.info("Content Page Object CHECK(" + contentPageObject.getName() + "- PASS");
    }

    @When( "I am verifying the report  %performanceReports is %visibleState" )
    public void iPerformanceReportsDisplayed(PerformanceReports performanceReports, Visibility visibleState){
        logger.info("Verifying Performance Report Objects :" + performanceReports.getName());
    }

    @Then( "pass the report %performanceReports check" )
    public void passPerformanceReport(PerformanceReports performanceReports){
        logger.info("Performance Chart CHECK(" + performanceReports.getName() + "- PASS");
    }

    @When( "I am verifying the %subHeaderPageObjects options are %visibleState" )
    public void subHeaderIsDisplayed(SubHeaderPageObjects subHeaderPageObjects, Visibility visibleState){
        logger.info("Verifying Sub Header Page object:" + subHeaderPageObjects.getName());
    }

    @Then( "pass the %subHeaderPageObjects options check" )
    public void passSubHeaderPageObjectsReport(SubHeaderPageObjects subHeaderPageObjects){
        logger.info("Sub Header CHECK(" + subHeaderPageObjects.getName() + "- PASS");
    }

    @When( "I am verifying the Page %headerPageObjects is %visibleState" )
    public void headerPageObjectsIsDisplayed(HeaderPageObjects headerPageObjects, Visibility visibleState){
        logger.info("Verifying Header Page Objects :" + headerPageObjects.getName());
    }

    @Then( "pass the Page %headerPageObjects check" )
    public void passHeaderPageObjectReport(HeaderPageObjects headerPageObjects){
        logger.info("Header pages Object CHECK" + headerPageObjects.getName() + "- PASS");
    }

    @When( "I am verifying the %contentPageObject is %visibleState" )
    public void isDisplayed(ContentPageObjects contentPageObject, Visibility visibleState){
        logger.info("Verifying Content Page Objects :" + contentPageObject.getName());
    }

    @Given( "the report metrics: %table" )
    public void verifyPerformanceMetrics(ExamplesTable table){
        logger.info("TableMetrics:" + table.getHeaders().size());
    }

    @When( "When my date is %date" )
    private void convertDate(String date){
        DateTimeFormatter fmt = DateTimeFormat.mediumDate();
        logger.info(" I have parsed teh date :" + fmt.parseDateTime(date).toString());
    }

    @Override
    public PageObject getPageObject(){
        return null;
    }

    @Override
    public void setWebPageObject(){

    }

    @AsParameterConverter
    public PerformanceReports performanceReports(String value){
        return (PerformanceReports)new PerformanceReportPageConverter().convertValue(value,String.class);
    }

    @AsParameterConverter
    public DateTime.Property currentMonth(String value){
        return (DateTime.Property)new CurrentMonthConverter().convertValue(value,String.class);
    }

    @AsParameterConverter
    public DashboardReports dashboardReport(String value){
        return (DashboardReports)new DashBoardReportsConverter().convertValue(value,String.class);
    }

    @AsParameterConverter
    public ApplicationPages applicationPages(String value){
        return (ApplicationPages)new ApplicationPageConverter().convertValue(value,String.class);
    }

    @AsParameterConverter
    public PerformanceMetrics performanceMetric(String value){
        return (PerformanceMetrics)new PerformanceMetricsConverter().convertValue(value,String.class);
    }

    @AsParameterConverter
    public DateTime epochDate(String value){
        return (DateTime)new EpochConverter().convertValue(value,String.class);
    }

    @AsParameterConverter
    public ContentPageObjects contentPageObject(String value){
        return (ContentPageObjects)new ContentPageObjectConverter().convertValue(value,String.class);
    }

    @AsParameterConverter
    public HeaderPageObjects headerPageObjects(String value){
        return (HeaderPageObjects)new HeaderPageObjectConverter().convertValue(value,String.class);
    }

    @AsParameterConverter
    public SubHeaderPageObjects subHeaderPageObjects(String value){
        return (SubHeaderPageObjects)new SubHeaderPageConverter().convertValue(value,String.class);
    }

    class SubHeaderPageConverter extends EnumConverter implements ParameterConverters.ParameterConverter{
        @Override
        public boolean accept(Type type){
            return type instanceof Class<?> && SubHeaderPageObjects.class.isAssignableFrom((Class<?>)type);
        }

        @Override
        public Object convertValue(String name, Type type){
            return SubHeaderPageObjects.valueOfByName(name);
        }
    }

    class HeaderPageObjectConverter extends EnumConverter implements ParameterConverters.ParameterConverter{
        @Override
        public boolean accept(Type type){
            return type instanceof Class<?> && HeaderPageObjects.class.isAssignableFrom((Class<?>)type);
        }

        @Override
        public Object convertValue(String name, Type type){
            return HeaderPageObjects.valueOfByName(name);
        }
    }
}


