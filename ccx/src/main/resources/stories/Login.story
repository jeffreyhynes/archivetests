
Scenario: Provide full details on how to log into CCX Portal

Given I am User assigned to the Issuer <issuer>
And I am a User assigned the Role <role>
When I open CCX Portal
Then the Login page is displayed

When I enter my username <username>
And I enter my password <password>
And I click the Login button
Then the Overview page is displayed

Examples:
|username           |password   |role           |issuer             |
|QA_SystemAdmin     |cards      |System Admin   |CreditCards.com    |

Scenario: Shortcut access for a user logging into CCX and the user is defined by their assigned Issuer and Role

Given I am User assigned to the Issuer <issuer>
And I am a User assigned the Role <role>
When I login as <username>

Examples:
|username           |password   |role           |issuer             |
|QA_SystemAdmin     |cards      |System Admin   |CreditCards.com    |
