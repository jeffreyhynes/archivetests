package com.creditcards.model;

public class OverviewSummary{
    private int newCards;
    private int savedCards;
    private int cardsWithErrors;
    private int cardsUpToDate;
    private int changesPending;
    private int deletionsPending;
    private int totalCards;

    public int getCardsUpToDate(){
        return cardsUpToDate;
    }

    public void setCardsUpToDate(int cardsUpToDate){
        this.cardsUpToDate = cardsUpToDate;
    }

    public int getCardsWithErrors(){
        return cardsWithErrors;
    }

    public void setCardsWithErrors(int cardsWithErrors){
        this.cardsWithErrors = cardsWithErrors;
    }

    public int getChangesPending(){
        return changesPending;
    }

    public void setChangesPending(int changesPending){
        this.changesPending = changesPending;
    }

    public int getDeletionsPending(){
        return deletionsPending;
    }

    public void setDeletionsPending(int deletionsPending){
        this.deletionsPending = deletionsPending;
    }

    public int getNewCards(){
        return newCards;
    }

    public void setNewCards(int newCards){
        this.newCards = newCards;
    }

    public int getSavedCards(){
        return savedCards;
    }

    public void setSavedCards(int savedCards){
        this.savedCards = savedCards;
    }

    public int getTotalCards(){
        return totalCards;
    }

    public void setTotalCards(int totalCards){
        this.totalCards = totalCards;
    }
}
