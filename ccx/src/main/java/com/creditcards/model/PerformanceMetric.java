package com.creditcards.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.builder.CompareToBuilder;

public class PerformanceMetric implements Comparable{
    private String imageName;
    private String title;
    private int value;

    public PerformanceMetric(){
    }

    public PerformanceMetric(String title, String value, String imageName){
        this.imageName = imageName;
        this.title = title;
        this.value = NumberUtils.toInt(value);
    }

    public String getImageName(){
        return imageName;
    }

    public void setImageName(String imageName){
        this.imageName = imageName;
    }

    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public int getValue(){
        return value;
    }

    public void setValue(String value){
        this.value = NumberUtils.toInt(value);
    }

    @Override
    public int hashCode(){
        return new HashCodeBuilder().append(this.imageName).append(this.title).append(this.value).toHashCode();
    }

    public boolean equals(final PerformanceMetric obj){
        return new EqualsBuilder().append(this.imageName,obj.imageName).append(this.title,obj.title)
                                  .append(this.value,obj.value).isEquals();
    }

    @Override
    public boolean equals(Object obj){
        if( obj == null || getClass() != obj.getClass() ){
            return false;
        }
        return equals((PerformanceMetric)obj);
    }

    public int compare(PerformanceMetric obj1, PerformanceMetric obj2){
        CompareToBuilder builder = new CompareToBuilder();
        builder.append(obj1.getTitle(),obj2.getTitle()).append(obj1.getValue(),obj2.getValue())
               .append(obj1.getImageName(),obj2.getImageName());
        return builder.toComparison();
    }

    public int compareTo(Object obj){
        return compareTo((PerformanceMetric)obj);
    }

    public int compareTo(PerformanceMetric timePeriod){
        CompareToBuilder builder = new CompareToBuilder();
        builder.append(title,timePeriod.getTitle()).append(value,timePeriod.getValue())
               .append(imageName,timePeriod.getImageName());
        return builder.toComparison();
    }
}
