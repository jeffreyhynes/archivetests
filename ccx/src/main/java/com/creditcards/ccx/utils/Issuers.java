package com.creditcards.ccx.utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import static org.apache.commons.lang3.StringUtils.EMPTY;

public enum Issuers{
    CREDITCARDS(0,"CreditCards.com","Credit-Cards-Logo-small.gif"),
    ST_FINANCIAL_BANK(134,"1st Financial Bank",EMPTY),
    ACCUCARD(61,"Accucard",EMPTY),
    ACHIEVECARD(152,"AchieveCard",EMPTY),
    ADVANTA(1,"Advanta",EMPTY),
    ALLY_BANK(160,"Ally Bank",EMPTY),
    AMERICAN_EXPRESS(2,"American Express",EMPTY),
    AMERICAN_OCTANE(145,"American Octane",EMPTY),
    AMEX_SAVINGS(171,"Amex Savings",EMPTY),
    APPLIED_BANK(81,"Applied Bank",EMPTY),
    ARC(87,"ARC",EMPTY),
    ASPIRE(62,"Aspire",EMPTY),
    ASSOCIATED_BANK(129,"Associated Bank",EMPTY),
    BANCORP(147,"Bancorp",EMPTY),
    BANK_FREEDOM(54,"Bank Freedom",EMPTY),
    BANK_OF_AMERICA(3,"Bank of America",EMPTY),
    BANK_OF_AMERICA_BUSINESS(174,"Bank of America Business",EMPTY),
    BANKFIRST(37,"BankFirst",EMPTY),
    BARCLAYCARD(46,"Barclaycard",EMPTY),
    BB_T(128,"BB&T ",EMPTY),
    BBVA_COMPASS(166,"BBVA Compass",EMPTY),
    BRYANT_STATE_BANK(146,"Bryant State Bank",EMPTY),
    CAPITAL_ONE(4,"Capital One",EMPTY),
    CCCOMUS_BANK(140,"CCCOMUS Bank",EMPTY),
    CHASE(5,"Chase",EMPTY),
    CHASEPAYMENTECH(45,"Chasepaymentech",EMPTY),
    CITI(6,"Citi",EMPTY),
    COMERICA(138,"Comerica",EMPTY),
    COMPUCREDIT(126,"CompuCredit",EMPTY),
    CONTINENTAL_FINANCE_COMPANY(164,"Continental Finance Company",EMPTY),
    CREDIT_ONE(117,"Credit One",EMPTY),
    CREDIT_ONE_BANK(132,"Credit One Bank",EMPTY),
    DEMO_BANK(88,"Demo Bank",EMPTY),
    DISCOVER(7,"Discover",EMPTY),
    DISCOVER_BANK(175,"Discover Bank",EMPTY),
    EDEBITPAY(63,"EDebitPay",EMPTY),
    ETRADE_BANK(115,"ETrade Bank",EMPTY),
    EUFORA(94,"Eufora",EMPTY),
    FIFTH_THIRD_BANK(125,"Fifth Third Bank",EMPTY),
    FINGERHUT(105,"Fingerhut",EMPTY),
    FIRST_BANK_OF_DELAWARE(34,"First Bank of Delaware",EMPTY),
    FIRST_NATIONAL_BANK_OF_OMAHA(27,"First National Bank Of Omaha",EMPTY),
    FIRST_NATIONAL_BANK_OMAHA(121,"First National Bank Omaha",EMPTY),
    FIRST_PREMIER_BANK(8,"First PREMIER Bank",EMPTY),
    FLEETCOR(163,"FLEETCOR",EMPTY),
    FNBO(114,"FNBO",EMPTY),
    FUTURA(106,"Futura",EMPTY),
    FYCINC(72,"FYCInc",EMPTY),
    GE_MONEY(33,"GE Money",EMPTY),
    GREEN_DOT(86,"Green Dot",EMPTY),
    HSBC_EXTERNAL(92,"HSBC-External",EMPTY),
    IBERIABANK(101,"Iberiabank",EMPTY),
    ICBA_BANCARD(135,"ICBA Bancard",EMPTY),
    ING_DIRECT(167,"ING DIRECT",EMPTY),
    KAIKU_FINANCE(165,"KAIKU Finance",EMPTY),
    LEADDRIVE(97,"LeadDrive",EMPTY),
    MANGO_FINANCIAL(109,"Mango Financial",EMPTY),
    MASTERCARD(10,"MasterCard",EMPTY),
    MERRICK_BANK(133,"Merrick Bank",EMPTY),
    MERRILL_LYNCH(49,"Merrill Lynch",EMPTY),
    METABANK(42,"MetaBank",EMPTY),
    METROPCS(100,"metroPCS",EMPTY),
    MODERN_CASH(158,"Modern Cash",EMPTY),
    NAVY_FCU(123,"Navy FCU",EMPTY),
    NETSPEND(30,"NetSpend",EMPTY),
    NEW_MILLENNIUM_BANK(29,"New Millennium Bank",EMPTY),
    NORDSTROM_BANK(118,"Nordstrom Bank",EMPTY),
    OBOPAY_BANCORP(98,"Obopay - Bancorp",EMPTY),
    ONEWEST_BANK(179,"OneWest Bank",EMPTY),
    ORCHARD_BANK(9,"Orchard Bank",EMPTY),
    OTHER_OFFERS(156,"Other Offers",EMPTY),
    PALM_DESERT_NATIONAL_BANK(47,"Palm Desert National Bank",EMPTY),
    PARTNERS_FIRST(142,"Partners First",EMPTY),
    PAYJR(39,"PAYjr",EMPTY),
    PAYPOWER(173,"PayPower",EMPTY),
    PENTAGON_FCU(130,"Pentagon FCU",EMPTY),
    PLAINS_COMMERCE_BANK(52,"Plains Commerce Bank",EMPTY),
    PNC_BANK(122,"PNC Bank",EMPTY),
    PRECASH(43,"Precash",EMPTY),
    PUBLIC_SAVINGS_BANK(108,"Public Savings Bank",EMPTY),
    PULASKI_BANK(38,"Pulaski Bank",EMPTY),
    RBS_CITIZENS(124,"RBS Citizens",EMPTY),
    READYDEBIT(66,"READYdebit",EMPTY),
    RUSHCARD(28,"RushCard",EMPTY),
    SCHWAB_BANK(116,"Schwab Bank",EMPTY),
    SIMMONS_FIRST_NATIONAL_BANK(36,"Simmons First National Bank",EMPTY),
    SIMPLETUITION(177,"SimpleTuition",EMPTY),
    STATE_FARM_BANK(127,"State Farm Bank",EMPTY),
    SUNTRUST_BANK(136,"SunTrust Bank",EMPTY),
    SYNOVUS_BANK(154,"Synovus Bank",EMPTY),
    TARGET(111,"target",EMPTY),
    THE_RETAIL_MERCHANT(96,"the Retail Merchant",EMPTY),
    TOWN_NORTH_BANK(131,"Town North Bank",EMPTY),
    TRANSUNION(64,"TransUnion",EMPTY),
    US_BANCORP(51,"U.S. Bancorp",EMPTY),
    UNIVERSITY_BANK(153,"University Bank",EMPTY),
    URBAN_TRUST_BANK(170,"Urban Trust Bank",EMPTY),
    US_BANK(113,"US Bank",EMPTY),
    USAA_SAVINGS(119,"USAA Savings",EMPTY),
    VETERANS_ADVANTAGE(155,"Veterans Advantage",EMPTY),
    VISA(11,"Visa",EMPTY),
    VISA_BLACK_CARD(176,"Visa Black Card",EMPTY),
    WASHINGTON_MUTUAL(40,"Washington Mutual",EMPTY),
    WELLS_FARGO(110,"Wells Fargo",EMPTY),
    WESTERN_ALLIANCE_BANCORPORATION(139,"Western Alliance Bancorporation",EMPTY),
    WESTERN_UNION(35,"Western Union",EMPTY),
    WORLDS_FOREMOST_BANK(112,"Worlds Foremost Bank",EMPTY),
    ZIONS_BANK(157,"Zions Bank",EMPTY);

    private final int id;
    private final String name;
    private final String logo;

    Issuers(int id, String name, String logo){
        this.id = id;
        this.name = name;
        this.logo = logo;
    }

    public int getId(){
        return id;
    }

    public String getName(){
        return name;
    }

    public static Issuers valueOfByName(String name){
        Issuers issuer = null;
        for( Issuers issuers : Issuers.values() ){
            if( StringUtils.equalsIgnoreCase(name,issuers.name) ){
                issuer = issuers;
                break;
            }
        }
        return issuer;
    }

    public static Issuers valueOfByID(String id){
        Issuers issuer = null;
        for( Issuers issuers : Issuers.values() ){
            if( NumberUtils.toInt(id) == issuers.getId()){
                issuer = issuers;
                break;
            }
        }
        return issuer;
    }
}
