package com.creditcards.ccx.utils;

import org.apache.commons.lang3.StringUtils;

public enum Users{
    QA_SYSTEM_ADMIN("QA_SystemAdmin","cards",Roles.ROLE_SYS_ADMIN,Issuers.CCCOMUS_BANK),
    QA_SYS_ADMIN("QA_SysAdmin","cards",Roles.ROLE_SYS_ADMIN,Issuers.CCCOMUS_BANK),
    QA_ADMIN("QA_Admin","cards",Roles.ADMIN,Issuers.CCCOMUS_BANK),

    BOA_ACCOUNT_MANAGER("BOA_AccountManager","cards",Roles.MANAGER,Issuers.BANK_OF_AMERICA),
    BOA_ACCOUNT_USER("BOA_AccountUser","cards",Roles.USER,Issuers.BANK_OF_AMERICA),
    BOA_BID_MONITOR("BOA_BidMonitor","cards",Roles.CC_MONITOR,Issuers.BANK_OF_AMERICA),
    BOA_BID_VIEWER("BOA_BidViewer","cards",Roles.CPBT_VIEWER,Issuers.BANK_OF_AMERICA),
    BOA_BIDDER("BOA_Bidder","cards",Roles.CPBT_BIDDER,Issuers.BANK_OF_AMERICA),

    ISSUER_BIDDER("Issuer_Bidder","cards",Roles.CPBT_BIDDER,Issuers.FIRST_PREMIER_BANK),
    ISSUER_MANAGER("Issuer_Manager","cards",Roles.MANAGER,Issuers.FIRST_PREMIER_BANK),
    ISSUER_MONITOR("Issuer_Monitor","cards",Roles.CC_MONITOR,Issuers.FIRST_PREMIER_BANK),
    ISSUER_USER("Issuer_User","cards",Roles.USER,Issuers.FIRST_PREMIER_BANK),
    ISSUER_VIEWER("Issuer_Viewer","cards",Roles.CPBT_VIEWER,Issuers.FIRST_PREMIER_BANK),

    METABANK_MANAGER("MetaBank_Manager","cards",Roles.MANAGER,Issuers.METABANK);

    private final String username;
    private final String password;
    private final Roles role;
    private final Issuers issuer;

    Users(String name, String userPassword, Roles roles, Issuers issuers){
        this.password = userPassword;
        this.username = name;
        this.role = roles;
        this.issuer = issuers;
    }

    public String getPassword(){
        return password;
    }

    public String getUsername(){
        return username;
    }

    public Roles getRole(){
        return role;
    }

    public Issuers getIssuer(){
        return issuer;
    }

    public static Users valueOfByName(String name){
        Users user = null;
        for( Users userType : Users.values() ){
            if( StringUtils.equalsIgnoreCase(name,userType.username) ){
                user = userType;
                break;
            }
        }
        return user;
    }

    public static Users valueOfByRole(Roles roles){
        Users user = null;
        for( Users users : Users.values() ){
            if( users.role == roles ){
                user = users;
                break;
            }
        }
        return user;
    }
}
