package com.creditcards.ccx.utils;

public enum ApplicationMetaData{
    TITLE("CCX - CreditCard Xchange");

    private final String value;

    private ApplicationMetaData(String value){
        this.value = value;
    }

    public String getValue(){
        return value;
    }
}
