package com.creditcards.ccx.pages.application;

import com.creditcards.pages.AbstractPageObject;
import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ReportDashboardPage extends AbstractPageObject{

    @FindBy(id = "container")
    private WebElement reportContainer;

    private enum PageElements{
        ID("container");

        private final String value;

        PageElements(String element){
            this.value = element;
        }

        public By byIDSelector(){
            return By.id(value);
        }

        public String toString(){
            return value;
        }
    }

    public ReportDashboardPage(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    @Override
    public By displayLocator(){
        return PageElements.ID.byIDSelector();
    }

    @Override
    public WebElement displayElement(){
        return reportContainer;
    }
}
