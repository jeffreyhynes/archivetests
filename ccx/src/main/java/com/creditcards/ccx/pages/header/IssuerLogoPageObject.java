package com.creditcards.ccx.pages.header;

import com.creditcards.pages.AbstractPageObject;
import org.apache.commons.lang3.StringUtils;
import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

class IssuerLogoPageObject extends AbstractPageObject{
    @FindBy(id = "issuer-logo")
    private WebElement logoContainer;

    @FindBys({ @FindBy(id = "issuer-logo"),@FindBy(tagName = "img") })
    private WebElement logoImage;

    public IssuerLogoPageObject(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    @Override
    public WebElement displayElement(){
        return logoImage;
    }

    @Override
    public By displayLocator(){
        return By.id("issuer-logo");
    }

    public boolean logoEquals(String logo){
        String imageURL = logoImage.getAttribute("src");
        return StringUtils.endsWith(imageURL,logo);
    }
}