package com.creditcards.ccx.pages.converters;

import com.creditcards.ccx.pages.content.ContentPageObjects;
import org.jbehave.core.steps.ParameterConverters;
import org.jbehave.core.steps.ParameterConverters.EnumConverter;

import java.lang.reflect.Type;

public class ContentPageObjectConverter extends EnumConverter implements ParameterConverters.ParameterConverter{
    @Override
    public boolean accept(Type type){
        return type instanceof Class<?> && ContentPageObjects.class.isAssignableFrom((Class<?>)type);
    }

    @Override
    public Object convertValue(String name, Type type){
        return ContentPageObjects.valueOfByName(name);
    }
}
