package com.creditcards.ccx.pages.header;

import com.creditcards.pages.AbstractContainerPageObject;
import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HeaderPageObject extends AbstractContainerPageObject{
    private static final Logger logger = LoggerFactory.getLogger(HeaderPageObject.class);

    @FindBy(id = "header")
    private WebElement headerContainer;

    private WelcomePageObject welcomePageElement;
    private IssuerLogoPageObject issuerLogoPageElement;
    private LogoPageObject logoPageElement;

    public HeaderPageObject(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    @Override
    public WebElement displayElement(){
        return headerContainer;
    }

    @Override
    public By displayLocator(){
        return By.id("header");
    }

    @Override
    public void initPageElements(){
        logger.info("Instantiating the children elements: Welcome Page, Issuer Logo, and Main Logo elements");
        welcomePageElement = new WelcomePageObject(getDriverProvider());
        issuerLogoPageElement = new IssuerLogoPageObject(getDriverProvider());
        logoPageElement = new LogoPageObject(getDriverProvider());

        issuerLogoPageElement.waitUntilLoaded();
        welcomePageElement.waitUntilLoaded();
        logoPageElement.waitUntilLoaded();
    }

    public boolean isLogoDisplayed(){
        return logoPageElement.isDisplayed();
    }

    public boolean isIssuerLogoDisplayed(){
        return issuerLogoPageElement.isDisplayed();
    }

    public boolean isWelcomeMessageDisplayed(){
        return welcomePageElement.isDisplayed();
    }

    public boolean isWelcomeMessageValid(String name, String role){
        return welcomePageElement.welcomeMessageMatches(name,role);
    }

    public void clickLogout(){
        if(welcomePageElement.isDisplayed()){
            welcomePageElement.clickLogout();
        }
    }
}