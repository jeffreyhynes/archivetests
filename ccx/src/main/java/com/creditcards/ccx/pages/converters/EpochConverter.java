package com.creditcards.ccx.pages.converters;

import org.jbehave.core.steps.ParameterConverters.ParameterConverter;
import org.joda.time.DateTime;
import org.joda.time.Instant;

import java.lang.reflect.Type;
import java.util.Calendar;

public class EpochConverter implements ParameterConverter{
    public EpochConverter(){
        Calendar.getInstance().get(Calendar.MONTH);
    }

    public boolean accept(Type type){
        return type instanceof Class<?> && Long.class.isAssignableFrom((Class<?>)type);
    }

    public Object convertValue(String value, Type type){
        Instant epochInstant = new Instant(value);
        DateTime date = epochInstant.toDateTime();
        return date;
    }
}

