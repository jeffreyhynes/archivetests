package com.creditcards.ccx.pages.reporting;

import com.creditcards.pages.AbstractPageObject;
import com.creditcards.util.Visibility;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

import java.util.ArrayList;
import java.util.List;

public class TopCardsByAccountPageObject extends AbstractPageObject{

    @FindBys({ @FindBy(id = "allReportsContainer"),@FindBy(className = "span4") })
    private WebElement reportContainer;

    @FindBy(id = "topcardsHeaderTitle")
    private WebElement overviewHeader;

    @FindBy(id = "dropdown-topcards-title")
    private WebElement overviewHeaderTitle;

    @FindBy(id = "topCardsDatePicker")
    private WebElement reportPicker;

    @FindBy(id = "piechart")
    private WebElement reportContent;

    @FindBys({ @FindBy(id = "piechart"),@FindBy(className = "highcharts-button") })
    private WebElement chartContextMenu;

    @FindBys({ @FindBy(id = "piechart"),@FindBy(className = "highcharts-series-group") })
    private WebElement chartSeriesGroup;

    @FindBys({ @FindBy(id = "piechart"),@FindBy(className = "highcharts-series-group"),@FindBy(tagName = "path") })
    private List<WebElement> chartSeries;

    @FindBys({ @FindBy(id = "piechart"),@FindBy(className = "highcharts-legend") })
    private WebElement chartLegend;

    @FindBys({ @FindBy(id = "piechart"),@FindBy(css = "div.highcharts-legend-item span") })
    private List<WebElement> legendCards;

    public enum ChartElement{
        PIECHART("piechart"),
        TITLE("Title"),
        LEGEND("Legend"),
        CONTEXT_MENU("Context Menu");

        private final String name;

        ChartElement(String name){
            this.name = name;
        }

        public String getName(){
            return this.name;
        }

        public static ChartElement valueOfByName(String name){
            ChartElement chartElement = null;
            for( ChartElement element : ChartElement.values() ){
                if( org.apache.commons.lang3.StringUtils.equalsIgnoreCase(name,element.name) ){
                    chartElement = element;
                    break;
                }
            }
            return chartElement;
        }
    }

    public TopCardsByAccountPageObject(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    @Override
    public By displayLocator(){
        return By.cssSelector("div#allReportsContainer div.span4");
    }

    @Override
    public WebElement displayElement(){
        return reportContainer;
    }

    public boolean chartElementVisibleStateMatches(ChartElement chartElement, Visibility visibility){
        boolean matchResult = false;

        switch( chartElement ){
            case TITLE:
                matchResult = elementIsDisplayed(overviewHeaderTitle);
                break;
            case CONTEXT_MENU:
                matchResult = elementIsDisplayed(chartContextMenu);
                break;
            case LEGEND:
                matchResult = elementIsDisplayed(chartLegend);
                break;
            case PIECHART:
                matchResult = elementIsDisplayed(reportContent);
                break;
        }
        return matchResult;
    }

    public boolean titleEquals(String expected){
        return StringUtils.equalsIgnoreCase(overviewHeaderTitle.getText(),expected);
    }

    public boolean accountCountMatch(int expected){
        return chartSeries.size() == expected;
    }

    public boolean assertCardListMatches(List<String> expectedList){
        List<String> actualList = new ArrayList<>();
        for( WebElement element : legendCards ){
            actualList.add(element.getText());
        }

        return ListUtils.intersection(expectedList,actualList).size() == expectedList.size();
    }
}
