package com.creditcards.ccx.pages.header;

import com.creditcards.pages.AbstractPageObject;
import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

class LogoPageObject extends AbstractPageObject{
    @FindBy(id = "logo-container")
    private WebElement logoContainer;

    @FindBys({ @FindBy(id = "logo-container"),@FindBy(tagName = "img") })
    private WebElement logoImage;

    public LogoPageObject(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    @Override
    public By displayLocator(){
        return By.id("logo-container");
    }

    @Override
    public WebElement displayElement(){
        return logoImage;
    }
}
