package com.creditcards.ccx.pages.application;

import com.creditcards.pages.AbstractPageObject;
import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class OverviewPage extends AbstractPageObject{

    @FindBy(id = "one-col-container")
    private WebElement primaryContainer;

    private enum PageElements{
        WELCOME("welcome");

        private final String value;

        PageElements(String element){
            this.value = element;
        }

        public By byIDSelector(){
            return By.id(value);
        }

        public String toString(){
            return value;
        }
    }

    public OverviewPage(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    @Override
    public By displayLocator(){
        return PageElements.WELCOME.byIDSelector();
    }

    @Override
    public WebElement displayElement(){
        return primaryContainer;
    }
}