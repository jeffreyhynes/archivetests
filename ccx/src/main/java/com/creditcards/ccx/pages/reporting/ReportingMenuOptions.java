package com.creditcards.ccx.pages.reporting;

import static org.apache.commons.lang3.StringUtils.EMPTY;

public enum ReportingMenuOptions{
    REPORT_DASHBOARD("Report Dashboard","reportDashboard.php",EMPTY),
    CLICKS("Clicks"," reporting.php","clicks"),
    ACCOUNTS("Accounts"," reporting.php","accounts"),
    CONVERSION_RATES("Conversion Rates"," reporting.php","conversion_rates"),
    TRANSACTION_DETAILS("Transaction Details"," reporting.php","transaction_details");

    private final String displayName;
    private final String href;
    private final String reportType;

    private ReportingMenuOptions(String displayName, String href, String report){
        this.displayName = displayName;
        this.href = href;
        this.reportType = report;
    }

    public String getDisplayName(){
        return displayName;
    }

    public String getHref(){
        return href;
    }

    public String getReportType(){
        return reportType;
    }
}
