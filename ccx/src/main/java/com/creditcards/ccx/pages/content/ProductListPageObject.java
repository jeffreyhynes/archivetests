package com.creditcards.ccx.pages.content;

import com.creditcards.pages.AbstractPageObject;
import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

public class ProductListPageObject extends AbstractPageObject{

    @FindBy(id = "cards")
    private WebElement cardContent;

    @FindBys({ @FindBy(id = "cards"),@FindBy(tagName = "table"),@FindBy(className = "yui-dt-data") })
    private WebElement cardList;

    public ProductListPageObject(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    @Override
    public WebElement displayElement(){
        return cardContent;
    }

    @Override
    public By displayLocator(){
        return By.id("cards");
    }
}
