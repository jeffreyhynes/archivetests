package com.creditcards.ccx.pages.content;

import org.apache.commons.lang3.StringUtils;

public enum ContentPageObjects{
    CARD_INTRODUCTION("Card Introduction"),
    PRODUCT_LIST("Product List");

    private final String name;

    private ContentPageObjects(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public static ContentPageObjects valueOfByName(String name){
        ContentPageObjects contentPageObject = null;
        for( ContentPageObjects page : ContentPageObjects.values() ){
            if( StringUtils.equalsIgnoreCase(name,page.name) ){
                contentPageObject = page;
                break;
            }
        }
        return contentPageObject;
    }
}
