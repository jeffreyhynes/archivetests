package com.creditcards.ccx.steps.header;

import com.creditcards.ccx.pages.header.HeaderPageObjects;
import com.creditcards.ccx.pages.header.MainNavigationTabPageObject;
import com.creditcards.steps.Steps;
import com.creditcards.util.Visibility;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.web.selenium.WebDriverProvider;
import org.springframework.beans.factory.annotation.Autowired;

@Steps
public class MainNavigationTabSteps extends HeaderPageObjectSteps{
    private MainNavigationTabPageObject mainNavigationTabPageObject;

    @Autowired
    public MainNavigationTabSteps(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    @Override
    public void setWebPageObject(){
        mainNavigationTabPageObject = (MainNavigationTabPageObject)getPageObject();
    }

    @Given("the Main Navigation Bar is %visibleState")
    @When("the Main Navigation Bar is %visibleState")
    @Then("the Main Navigation Bar is %visibleState")
    public void visibleState(Visibility visibleState){
        super.visibleState(HeaderPageObjects.MAIN_NAVIGATION,visibleState);
        setWebPageObject();
    }

    @When("I click the Main Navigation Bar Reporting tab")
    public void clickReportingTab(){
        mainNavigationTabPageObject.openReportDashboard();
    }
}
