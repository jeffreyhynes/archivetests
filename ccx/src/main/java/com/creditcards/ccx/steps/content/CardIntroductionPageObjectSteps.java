package com.creditcards.ccx.steps.content;

import com.creditcards.ccx.pages.content.CardIntroductionPageObject;
import com.creditcards.ccx.pages.content.ContentPageObjects;
import com.creditcards.steps.Steps;
import com.creditcards.util.Visibility;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.text.StrBuilder;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.web.selenium.WebDriverProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import static org.testng.Assert.assertTrue;

@Steps
public class CardIntroductionPageObjectSteps extends PageSteps{
    private static final Logger logger = LoggerFactory.getLogger(CardIntroductionPageObjectSteps.class);

    private CardIntroductionPageObject introductionPageObject;

    @Autowired
    public CardIntroductionPageObjectSteps(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    @When("the Card Introduction is %visibleState")
    @Then("the Card Introduction is %visibleState")
    public void verifyPageObjectVisibility(Visibility visibleState){
        isDisplayed(ContentPageObjects.CARD_INTRODUCTION,visibleState);
        setWebPageObject();
    }

    @Override
    public void setWebPageObject(){
        introductionPageObject = (CardIntroductionPageObject)getPageObject();
    }

    @Then("the Card Introduction conatins the text %filename")
    public void contentMatches(String filename){
        URL baseDirURL = FileUtils.class.getResource("/");
        String basePath = FilenameUtils.getFullPathNoEndSeparator(baseDirURL.getPath());

        StrBuilder builder = new StrBuilder();
        builder.append(basePath).append(IOUtils.DIR_SEPARATOR).append(filename);
        File dataFile = new File(builder.toString());
        try{
            String content = FileUtils.readFileToString(dataFile);
            assertTrue(introductionPageObject.textContentMatches(content),
                       "Card Introduction Content does not match the expected value.");
        }catch( IOException ioe ){
            logger.error("Exception finding file",ioe);
        }
    }
}