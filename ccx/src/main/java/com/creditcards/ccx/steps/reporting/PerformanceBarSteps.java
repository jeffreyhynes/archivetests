package com.creditcards.ccx.steps.reporting;

import com.creditcards.ccx.pages.reporting.PerformanceBarPageObject;
import com.creditcards.ccx.pages.reporting.PerformanceBarPageObject.MetaPageData;
import com.creditcards.ccx.pages.reporting.PerformanceReports;
import com.creditcards.lang.StringCharacter;
import com.creditcards.model.PerformanceMetric;
import com.creditcards.steps.Steps;
import com.creditcards.util.Visibility;
import org.apache.commons.lang3.text.StrBuilder;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;
import org.jbehave.web.selenium.WebDriverProvider;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;
import java.util.Vector;

import static org.testng.Assert.assertTrue;

@Steps
public class PerformanceBarSteps extends PerformanceReportSteps{
    PerformanceBarPageObject performanceBarPageObject;

    enum MetaData{
        TITLE("title"),
        IMAGE("image"),
        VALUE("value"),
        TITLE_SUFFIX("Performance to Date");
        private final String value;

        MetaData(String value){
            this.value = value;
        }
    }

    @Autowired
    public PerformanceBarSteps(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    @Override
    public void setWebPageObject(){
        performanceBarPageObject = (PerformanceBarPageObject)getPageObject();
    }

    @Given( "the report Performance Bar is %visibleState" )
    @When( "the report Performance Bar is %visibleState" )
    @Then( "the report Performance Bar is %visibleState" )
    public void visibleState(Visibility visibleState){
        super.visibleState(PerformanceReports.PERFORMANCE_BAR,visibleState);
        setWebPageObject();
    }

    @Then( "the report Performance Bar title displays the %current_month" )
    public void reportTitleForCurrentMonth(DateTime.Property current_month){
        String title = buildHeaderTitle(current_month);
        assertTrue(performanceBarPageObject.elementTextMatches(MetaPageData.TITLE,title));
    }

    @Then( "the Performance Bar metrics are: %table" )
    public void verifyPerformanceMetrics(ExamplesTable table){
        List<PerformanceMetric> metricList = buildPerformanceMetricList(table);
        assertTrue(performanceBarPageObject.performanceMetricsEquals(metricList));
    }

    private String buildHeaderTitle(DateTime.Property currentMonth){
        DateTime.Property currentYear = new DateTime().year();
        StrBuilder builder = new StrBuilder();
        builder.append(currentMonth.getAsShortText()).append(StringCharacter.SPACE).append(currentYear.getAsText())
               .append(StringCharacter.SPACE).append(MetaData.TITLE_SUFFIX.value);
        return builder.toString();
    }

    private List<PerformanceMetric> buildPerformanceMetricList(ExamplesTable table){
        List<PerformanceMetric> metricList = new Vector<>();
        List<Map<String, String>> rows = table.getRows();
        for( Map<String, String> listDetails : rows ){
            PerformanceMetric metric = new PerformanceMetric();
            metricList.add(metric);

            metric.setTitle(listDetails.get(MetaData.TITLE.value));
            metric.setImageName(listDetails.get(MetaData.IMAGE.value));
            metric.setValue(listDetails.get(MetaData.VALUE.value));
        }
        return metricList;
    }
}