package com.creditcards.ccx.steps.application;

import com.creditcards.ccx.pages.PageManager;
import com.creditcards.ccx.pages.application.ApplicationPages;
import com.creditcards.ccx.pages.converters.ApplicationPageConverter;
import com.creditcards.pages.PageObject;
import com.creditcards.steps.AbstractSteps;
import com.creditcards.steps.Steps;
import com.creditcards.util.Visibility;
import org.jbehave.core.annotations.AsParameterConverter;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.web.selenium.WebDriverProvider;
import org.springframework.beans.factory.annotation.Autowired;

import static org.testng.Assert.assertTrue;

@Steps
public abstract class PageSteps extends AbstractSteps{
    private PageObject pageObject;

    @Autowired
    private PageManager pageManager;

    @Autowired
    protected PageSteps(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    public PageManager getPageManager(){
        return pageManager;
    }

    @Given("I am on the %applicationPages pages")
    @When("I am on the %applicationPages pages")
    @Then("I am on the %applicationPages pages")
    public void verifyPageIsDisplayed(ApplicationPages applicationPage){
        pageObject = pageManager.getApplicationPageObject(applicationPage);
        assertTrue(pageObject.isDisplayed());
    }

    @When("the %applicationPages pages is %visibleState")
    @Then("the %applicationPages pages is %visibleState")
    public void visibleState(ApplicationPages applicationPage, Visibility visibleState){
        pageObject = pageManager.getApplicationPageObject(applicationPage);
        switch( visibleState ){
            case DISPLAYED:
                pageObject.waitUntilLoaded();
                break;
            case HIDDEN:
                pageObject.waitUntilUnLoaded();
                break;
        }

        assertTrue(pageObject.visibleStateMatches(visibleState),"The Expected pages was not displayed.");
    }

    public void getApplicationWebPageObject(ApplicationPages applicationPage){
        pageObject = pageManager.getApplicationPageObject(applicationPage);
        setWebPageObject();
    }

    @Override
    public PageObject getPageObject(){
        return pageObject;
    }

    @AsParameterConverter
    public ApplicationPages applicationPages(String value){
        return (ApplicationPages)new ApplicationPageConverter().convertValue(value,String.class);
    }
}