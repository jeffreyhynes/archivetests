package com.creditcards.ccx.steps.content;

import com.creditcards.ccx.pages.PageManager;
import com.creditcards.ccx.pages.content.ContentPageObjects;
import com.creditcards.pages.PageObject;
import com.creditcards.steps.AbstractSteps;
import com.creditcards.steps.Steps;
import com.creditcards.util.Visibility;
import org.jbehave.web.selenium.WebDriverProvider;
import org.springframework.beans.factory.annotation.Autowired;

import static org.testng.Assert.assertTrue;

@Steps
public abstract class PageSteps extends AbstractSteps{
    private PageObject pageObject;

    @Autowired
    private PageManager pageManager;

    @Autowired
    public PageSteps(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    public PageManager getPageManager(){
        return pageManager;
    }

    @Override
    public PageObject getPageObject(){
        return pageObject;
    }

    public void isDisplayed(ContentPageObjects contentPageObject, Visibility visibleState){
        pageObject = pageManager.getContentPageObject(contentPageObject);
        switch( visibleState ){
            case DISPLAYED:
                pageObject.waitUntilLoaded();
                break;
            case HIDDEN:
                pageObject.waitUntilUnLoaded();
                break;
        }
        assertTrue(pageObject.visibleStateMatches(visibleState));
    }
}
