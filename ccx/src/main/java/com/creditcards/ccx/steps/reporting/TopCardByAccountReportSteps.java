package com.creditcards.ccx.steps.reporting;

import com.creditcards.ccx.pages.reporting.PerformanceReports;
import com.creditcards.ccx.pages.reporting.TopCardsByAccountPageObject;
import com.creditcards.ccx.pages.reporting.TopCardsByAccountPageObject.ChartElement;
import com.creditcards.steps.Steps;
import com.creditcards.util.Visibility;
import org.jbehave.core.annotations.AsParameterConverter;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;
import org.jbehave.core.steps.ParameterConverters;
import org.jbehave.core.steps.ParameterConverters.EnumConverter;
import org.jbehave.web.selenium.WebDriverProvider;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import static org.testng.Assert.assertTrue;

@Steps
public class TopCardByAccountReportSteps extends PerformanceReportSteps{
    TopCardsByAccountPageObject reportPageObject;

    private static final String REPORT_COLUMN = new String("cardname");

    @Autowired
    public TopCardByAccountReportSteps(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    @Override
    public void setWebPageObject(){
        reportPageObject = (TopCardsByAccountPageObject)getPageObject();
    }

    @Given("the report Top Cards by Accounts is %visibleState")
    @When("the report Top Cards by Accounts is %visibleState")
    @Then("the report Top Cards by Accounts is %visibleState")
    public void visibleState(Visibility visibleState){
        super.visibleState(PerformanceReports.TOP_CARDS,visibleState);
        setWebPageObject();
    }

    @Then("the report Top Cards by Accounts %chartElement is %visibleState")
    public void chartElementVisibleState(ChartElement chartElement, Visibility visibleState){
        assertTrue(reportPageObject.chartElementVisibleStateMatches(chartElement,visibleState));
    }

    @Then("the report Top Cards by Accounts title displays the %current_month")
    public void verifyChartTitle(DateTime.Property current_month){
        String title = PerformanceReports.TOP_CARDS.buildHeaderTitle(current_month);
        assertTrue(reportPageObject.titleEquals(title));
    }

    @Then("the report Top Cards by Accounts has %count piechart divisions")
    public void verifyCardChartCount(int count){
        assertTrue(reportPageObject.accountCountMatch(count));
    }

    @Then("the report Top Cards by Accounts legend includes the cards: %cardList")
    public void verifyChartCardList(ExamplesTable cardList){
        assertTrue(reportPageObject.assertCardListMatches(buildCardList(cardList)));
    }

    private List<String> buildCardList(ExamplesTable table){
        List<String> cardList = new Vector<>();
        List<Map<String, String>> rows = table.getRows();
        for( Map<String, String> listDetails : rows ){
            cardList.add(listDetails.get(REPORT_COLUMN));
        }
        return cardList;
    }

    @AsParameterConverter
    public TopCardsByAccountPageObject.ChartElement chartElement(String value){
        return (TopCardsByAccountPageObject.ChartElement)new TopCardByAccountElementConverter()
                .convertValue(value,String.class);
    }

    class TopCardByAccountElementConverter extends EnumConverter implements ParameterConverters.ParameterConverter{
        @Override
        public boolean accept(Type type){
            return type instanceof Class<?> && TopCardsByAccountPageObject.ChartElement.class
                    .isAssignableFrom((Class<?>)type);
        }

        @Override
        public Object convertValue(String name, Type type){
            return TopCardsByAccountPageObject.ChartElement.valueOfByName(name);
        }
    }
}