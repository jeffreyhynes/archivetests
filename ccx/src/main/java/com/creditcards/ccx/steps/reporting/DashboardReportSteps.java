package com.creditcards.ccx.steps.reporting;

import com.creditcards.ccx.pages.reporting.DashboardReportPageObject;
import com.creditcards.ccx.pages.reporting.DashboardReportPageObject.ChartElement;
import com.creditcards.ccx.pages.reporting.DashboardReportPageObject.DashboardReports;
import com.creditcards.ccx.pages.reporting.PerformanceReports;
import com.creditcards.steps.Steps;
import com.creditcards.util.Selection;
import com.creditcards.util.Visibility;
import org.jbehave.core.annotations.AsParameterConverter;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;
import org.jbehave.core.steps.ParameterConverters;
import org.jbehave.core.steps.ParameterConverters.EnumConverter;
import org.jbehave.web.selenium.WebDriverProvider;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import static org.testng.Assert.assertTrue;

@Steps
public class DashboardReportSteps extends PerformanceReportSteps{
    DashboardReportPageObject reportPageObject;

    private static final String REPORT_COLUMN = new String("report_name");

    @Autowired
    public DashboardReportSteps(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    @Override
    public void setWebPageObject(){
        reportPageObject = (DashboardReportPageObject)getPageObject();
    }

    @Given("the report Dashboard Report is %visibleState")
    @When("the report Dashboard Report is %visibleState")
    @Then("the report Dashboard Report is %visibleState")
    public void visibleState(Visibility visibleState){
        super.visibleState(PerformanceReports.DASHBOARD_REPORT,visibleState);
        setWebPageObject();
    }

    @Then("the report Dashboard Report report selection includes the reports: %reports")
    public void verifyChartSelections(ExamplesTable table){
        List<String> reportList = buildReportList(table);
        assertTrue(reportPageObject.reportSelectionsEquals(reportList));
    }

    @Then("the %dashboardReports Dashboard Report is %selectedState")
    public void chartSelection(DashboardReports dashboardReport, Selection selectedState){
        assertTrue(selectedState.isSelected() && reportPageObject.reportIsSelected(dashboardReport));
    }

    @Then("the report Dashboard Report %chartElement is %visibleState")
    public void chartElementVisibleState(ChartElement chartElement, Visibility visibleState){
        assertTrue(reportPageObject.chartElementVisibleStateMatches(chartElement,visibleState));
    }

    @Then("the report Dashboard Report has a y-axis label of %label")
    public void verifyChartAxisLabel(String label){
        assertTrue(reportPageObject.yAxisTextMatches(label));
    }

    @Then("the report Dashboard Report has a date range  from <report_from_date> to <report_to_date>")
    public void verifyDateRange(@Named("report_from_date") String report_from_date,
                                @Named("report_to_date") String report_to_date){
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM);
        try{
            assertTrue(reportPageObject
                               .dateRangeMatches(dateFormat.parse(report_from_date),dateFormat.parse(report_to_date)));
        }catch( ParseException pe ){

        }
    }

    private List<String> buildReportList(ExamplesTable table){
        List<String> reportList = new Vector<>();
        List<Map<String, String>> rows = table.getRows();
        for( Map<String, String> listDetails : rows ){
            reportList.add(listDetails.get(REPORT_COLUMN));
        }
        return reportList;
    }

    @AsParameterConverter
    public ChartElement chartElement(String value){
        return (ChartElement)new DashboardReportChartElementConverter().convertValue(value,String.class);
    }

    class DashboardReportChartElementConverter extends EnumConverter implements ParameterConverters.ParameterConverter{
        @Override
        public boolean accept(Type type){
            return type instanceof Class<?> && ChartElement.class.isAssignableFrom((Class<?>)type);
        }

        @Override
        public Object convertValue(String name, Type type){
            return ChartElement.valueOfByName(name);
        }
    }
}