package com.creditcards.ccx.steps.header;

import com.creditcards.ccx.pages.PageManager;
import com.creditcards.ccx.pages.converters.HeaderPageObjectConverter;
import com.creditcards.ccx.pages.header.HeaderPageObjects;
import com.creditcards.pages.PageObject;
import com.creditcards.steps.AbstractSteps;
import com.creditcards.steps.Steps;
import com.creditcards.util.Visibility;
import org.jbehave.core.annotations.AsParameterConverter;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.web.selenium.WebDriverProvider;
import org.springframework.beans.factory.annotation.Autowired;

import static org.testng.Assert.assertTrue;

@Steps
public abstract class HeaderPageObjectSteps extends AbstractSteps{
    private PageObject pageObject;

    @Autowired
    private PageManager pageManager;

    @Autowired
    public HeaderPageObjectSteps(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    @Override
    public PageObject getPageObject(){
        return pageObject;
    }

    @Given("the Page %headerPageObjects is %visibleState")
    @When("the Page %headerPageObjects is %visibleState")
    @Then("the Page %headerPageObjects is %visibleState")
    public void visibleState(HeaderPageObjects headerPageObjects, Visibility visibleState){
        pageObject = pageManager.getHeaderPageObject(headerPageObjects);
        switch( visibleState ){
            case DISPLAYED:
                pageObject.waitUntilLoaded();
                break;
            case HIDDEN:
                pageObject.waitUntilUnLoaded();
                break;
        }
        assertTrue(pageObject.visibleStateMatches(visibleState));
    }

    @AsParameterConverter
    public HeaderPageObjects headerPageObjects(String value){
        return (HeaderPageObjects)new HeaderPageObjectConverter().convertValue(value,String.class);
    }
}