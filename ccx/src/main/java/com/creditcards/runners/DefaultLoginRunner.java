package com.creditcards.runners;

import com.creditcards.AbstractEmbedderRunner;
import com.creditcards.io.StoryLoader;
import com.creditcards.webdriver.StoryWebDriverProvider;
import org.jbehave.core.annotations.spring.UsingSpring;
import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.embedder.StoryControls;
import org.jbehave.core.failures.FailingUponPendingStep;
import org.jbehave.core.failures.PendingStepStrategy;
import org.jbehave.core.i18n.LocalizedKeywords;
import org.jbehave.core.io.LoadFromClasspath;
import org.jbehave.core.junit.spring.SpringAnnotatedEmbedderRunner;
import org.jbehave.core.model.ExamplesTableFactory;
import org.jbehave.core.reporters.CrossReference;
import org.jbehave.core.reporters.Format;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.steps.ParameterConverters;
import org.jbehave.web.selenium.ContextView;
import org.jbehave.web.selenium.LocalFrameContextView;
import org.jbehave.web.selenium.SeleniumConfiguration;
import org.jbehave.web.selenium.SeleniumContext;
import org.jbehave.web.selenium.SeleniumContextOutput;
import org.jbehave.web.selenium.SeleniumStepMonitor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.SimpleDateFormat;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static org.jbehave.core.io.CodeLocations.codeLocationFromClass;
import static org.jbehave.core.reporters.Format.CONSOLE;
import static org.jbehave.web.selenium.WebDriverHtmlOutput.WEB_DRIVER_HTML;

@RunWith(SpringAnnotatedEmbedderRunner.class)
@UsingSpring(resources = "application-configuration.xml")
public class DefaultLoginRunner extends AbstractEmbedderRunner{
    private static final Logger logger = LoggerFactory.getLogger(DefaultLoginRunner.class);
    private final StoryLoader storyLoader = new StoryLoader();

    @Autowired
    private StoryWebDriverProvider webDriverProvider;

    @Test
    public void run() throws Throwable{
        logger.info("****************** Running the CardSynergy Manager Story Runner");
        PendingStepStrategy pendingStepStrategy = new FailingUponPendingStep();
        CrossReference crossReference = new CrossReference().withJsonOnly().withPendingStepStrategy(pendingStepStrategy)
                                                            .withOutputAfterEachStory(true)
                                                            .excludingStoriesWithNoExecutedScenarios(true);

        ParameterConverters parameterConverters = new ParameterConverters();
        ExamplesTableFactory examplesTableFactory = new ExamplesTableFactory(new LocalizedKeywords(),
                                                                             new LoadFromClasspath(
                                                                                     DefaultLoginRunner.class),
                                                                             parameterConverters);
        parameterConverters.addConverters(new ParameterConverters.DateConverter(new SimpleDateFormat("MMM d, yyyy")),
                                          new ParameterConverters.ExamplesTableConverter(examplesTableFactory),
                                          new ParameterConverters.EnumConverter());

        ContextView contextView = new LocalFrameContextView().sized(640,120);
        SeleniumContext seleniumContext = new SeleniumContext();
        SeleniumStepMonitor stepMonitor = new SeleniumStepMonitor(contextView,seleniumContext,
                                                                  crossReference.getStepMonitor());
        Format[] formats = new Format[]{ new SeleniumContextOutput(seleniumContext),CONSOLE,WEB_DRIVER_HTML };

        StoryReporterBuilder reporterBuilder = new StoryReporterBuilder()
                .withCodeLocation(codeLocationFromClass(DefaultLoginRunner.class)).withFailureTrace(true)
                .withFailureTraceCompression(true).withFormats(formats).withCrossReference(crossReference);

        Configuration configuration = injectedEmbedder().configuration();
        configuration.useFailureStrategy(new FailingUponPendingStep())
                     .useStoryControls(new StoryControls().doResetStateBeforeScenario(false))
                     .useStepMonitor(stepMonitor).useStoryLoader(new LoadFromClasspath(DefaultLoginRunner.class))
                     .useStoryReporterBuilder(reporterBuilder).useParameterConverters(parameterConverters);

        SeleniumConfiguration seleniumConfiguration = (SeleniumConfiguration)configuration;
        seleniumConfiguration.useSeleniumContext(seleniumContext);
        seleniumConfiguration.useWebDriverProvider(webDriverProvider);

        injectedEmbedder().runStoriesAsPaths(storyLoader.storyPaths(getIncludes(),getExcludes()));
    }

    protected List<String> getIncludes(){
        return newArrayList("**/roleAccess/cccomus/DefaultLoginDisplay.story");
        //return newArrayList("**/ReportDashboard.story");
        //return newArrayList("**/Login.story");
    }

    protected List<String> getExcludes(){
        return null;
    }
}
